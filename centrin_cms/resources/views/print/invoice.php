<table style=" width:100%">
	<tr>
  	  <td style="width:50%"><img src="https://pbs.twimg.com/profile_images/487129866811084801/Gp4Ep8SN.jpeg" width="100" ></td>
    	<td align="right" style="width:50%; font-size:30px">INVOICE<br><small style="color: #999999; font-size:16;"> #<?php echo $order->order_number;?></small></td>
    </tr>
    <tr>
   	  <td></td>
        <td align="right">
        	<br><br><br><br>
        	<?php echo $order->customer_name;?><br>
        	<?php echo $order->customer_address;?><br>
        	<?php echo $order->phone;?>            
      	</td>
    </tr>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<table border="1" cellpadding="0" cellspacing="0" style="  border: 1px solid #dddddd; width:100%; ">
	<tr style="border: 1px solid #dddddd;">
    	<th style="padding: 8px;">Product Name</th>
    	<th>Quantity</th>
    	<th>Price</th>
    	<th>Total</th>
    </tr>
    <?php
		$total = 0;
		foreach ($orderItems as $row){
	?>
  <tr style="border: 1px solid #dddddd; padding: 8px;">
        	<td style=" padding: 8px;"><?php echo $row->product_name;?></td>
   	  <td align="right"><?php echo $row->total;?></td>
   	  <td align="right"><?php echo $row->price;?></td>
        	<td align="right"><?php echo $row->total*$row->price;?></td>
        </tr>
    <?php
			$total += $row->total*$row->price;
		}
	?>
	<tr>
    	<td colspan="2"  style=" padding: 8px;">&nbsp;</td>
    	<td  align="right"><strong>Total</strong></td>
    	<td align="right"><?php echo $total;?></td>
    </tr>
</table>