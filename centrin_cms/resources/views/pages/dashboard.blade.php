@extends('ib')

@section('content')

<script src="scripts/common.js" type="text/javascript"></script>
<div class="span9">
                        <div class="content">
                            <!--/#btn-controls-->
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Summary</h3>
                                </div>
                                <div class="module-body">
                                    <div class="chart inline-legend grid">
                                        <div id="placeholder2" class="graph" style="height: 500px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.module-->
                            <div class="module hide">
                                <div class="module-head">
                                    <h3>
                                        Adjust Budget Range</h3>
                                </div>
                                <div class="module-body">
                                    <div class="form-inline clearfix">
                                        <a href="#" class="btn pull-right">Update</a>
                                        <label for="amount">
                                            Price range:</label>
                                        &nbsp;
                                        <input type="text" id="amount" class="input-" />
                                    </div>
                                    <hr />
                                    <div class="slider-range">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Last Logged In Users</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No
                                                </th>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Logged In
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$counter=1;
											foreach ($lastLoggedInUsers as $row)
											{
												echo '<tr class="odd gradeX">
                                                <td>
                                                    '.$counter.'
                                                </td>
                                                <td>
                                                    '.$row->user_name.'
                                                </td>
                                                <td>
                                                    '.$row->email.'
                                                </td>
                                                <td class="center">
                                                    '.$row->last_login.'
                                                </td>
                                            </tr>';
												$counter++;	
											}
										?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Most Active Users</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No
                                                </th>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Total Product
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$counter=1;
											foreach ($mostActiveUsers as $row)
											{
												echo '<tr class="odd gradeX">
                                                <td>
                                                    '.$counter.'
                                                </td>
                                                <td>
                                                    '.$row->user_name.'
                                                </td>
                                                <td>
                                                    '.$row->email.'
                                                </td>
                                                <td class="center">
                                                    '.$row->total.'
                                                </td>
                                            </tr>';
												$counter++;	
											}
										?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Category Summary</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No
                                                </th>
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Category Indo
                                                </th>
                                                <th>
                                                    Total Product
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$counter=1;
											foreach ($categorySummaries as $row)
											{
												echo '<tr class="odd gradeX">
                                                <td>
                                                    '.$counter.'
                                                </td>
                                                <td>
                                                    '.$row->cat_name.'
                                                </td>
                                                <td>
                                                    '.$row->cat_name_indo.'
                                                </td>
                                                <td class="center">
                                                    '.$row->total.'
                                                </td>
                                            </tr>';
												$counter++;	
											}
										?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
<script type="text/javascript">
$(document).ready(function () {
	//var d1 = [ [new Date('2011-01-01').getTime(), 1], [new Date('2011-01-02').getTime(), 14] ];
	//var d2 = [ [new Date('2011-01-01').getTime(), 5], [new Date('2011-01-02').getTime(), 2]];
    <?php echo($d1);?>
	
	
    <?php echo($d2);?>
	
	
    <?php echo($d3);?>
	 
		var plot = $.plot($('#placeholder2'),
			   [ { data: d1, label: 'New Product'}, { data: d2, label: 'Transaction' },  { data: d3, label: 'New User' } ], {
					eries: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                colors: ["#37b7f3", "#d12610", "#52e136"],
                xaxis: {
                     mode: "time", timeformat: "%d/%m/%y", minTickSize: [1, "day"]
                }
				 });

		function showTooltip(x, y, contents) {
			$('<div id="gridtip">' + contents + '</div>').css( {
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5
			}).appendTo('body').fadeIn(300);
		}

		var previousPoint = null;
		$('#placeholder2').bind('plothover', function (event, pos, item) {
			$('#x').text(pos.x.toFixed(2));
			$('#y').text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$('#gridtip').remove();
					var x = item.datapoint[0].toFixed(0),
						y = item.datapoint[1].toFixed(0);

					showTooltip(item.pageX, item.pageY,
								' Total : ' + y);
				}
			}
			else {
				$('#gridtip').remove();
				previousPoint = null;
			}
		});
});
</script>
@stop
