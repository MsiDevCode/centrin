@extends('ib')

@section('content')

<div class="span9">
 	<div class="content">
                            <!--/#btn-controls-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Sales</h3>
                                </div>
                                <div class="module-body">
                                	<div id="container"></div>
                                </div>
                           	</div>
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Product Summary</h3>
                                </div>
                                
                                <div class="module-body">
                                    <div id="stockchart" style="width: 100%; height: 500px;"></div>
                                    <div id="statuschart" style="width: 100%; height: 500px;"></div>
                                    <div id="categorysummary" style="width: 100%; height: 600px;"></div>
                                </div>
                      		</div>
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Most Active Seller</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No
                                                </th>
                                                <!--
                                                <th>
                                                    Seller Name
                                                </th>-->
                                                <th>
                                                    Seller Email
                                                </th>
                                                <th>
                                                    Total Product
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$counter=1;
											foreach ($mostActiveUsers as $row)
											{
												echo '<tr class="odd gradeX">
                                                <td>
                                                    '.$counter.'
                                                </td>
												<!--
                                                <td>
                                                    '.$row->first_name.' '.$row->last_name.'
                                                </td>
												-->
                                                <td>
                                                    '.$row->email.'
                                                </td>
                                                <td class="center">
                                                    '.$row->total.'
                                                </td>
                                            </tr>';
												$counter++;	
											}
										?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            
	</div>
</div>
        <script src="{{ URL::asset('scripts/highcharts.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/highcharts-3d.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/exporting.js') }}" type="text/javascript"></script>
<script>
$(function () {
    $('#stockchart').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Product Stock'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y} ( {point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Total',
            data: [
                <?php
					foreach ($stockReports as $row)
					{
						echo "['".$row->stock_type."',".$row->total."],";
					}
				?>
            ]
        }]
    });
	
Highcharts.setOptions({
     colors: ['#ED561B', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
    });
    $('#statuschart').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Product Status'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y} ( {point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Total',
            data: [
                <?php
					foreach ($statusReports as $row)
					{
						echo "['".$row->status."',".$row->total."],";
					}
				  ?>
            ]
        }]
    });
	
	
       $('#categorysummary').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Category Summary'
        },
        xAxis: {
            categories: [
                <?php
				foreach ($categoryReports as $row)
				{
					echo "'".$row->cat_name."',";
				}
				?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Product'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Category',
            data: [
				<?php
				$counter=0;
				foreach ($categoryReports as $row)
				{
					echo "".$row->total.",";
				}
				?>
			]

        }]
    });
	
	
	$('#container').highcharts({
        title: {
            text: 'Sales Report',
            x: -20 //center
        },
        xAxis: {
            categories: [
			<?php
				foreach ($orderReports as $row)
				{
					echo "'".$row->order_date."',";
				}
			?>			
			]
        },
        yAxis: {
            title: {
                text: 'Total Order'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'New Order',
            data: [
			<?php
				foreach ($orderReports as $row)
				{
					echo $row->total.",";
				}
			?>		
			]
        }]
    });
	
});
</script>
@stop