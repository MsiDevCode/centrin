@extends('ib')

@section('content')

        <link type="text/css" href="{{ URL::asset('css/file-validator.css') }}" rel="stylesheet">
        <script src="{{ URL::asset('scripts/jquery.validate.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/additional-methods.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/file-validator.js') }}" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
				$('.filevalidator').fileValidator({
					onValidation: function(files){      $(this).attr('class',''); $('#buttonsubmit').removeAttr('disabled');         },
					onInvalid:    function(type, file){ $(this).addClass('invalid '+type); $('#buttonsubmit').attr('disabled','disabled'); }
				  });
			});
        </script>
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Create Product</h3>
							</div>
							<div class="module-body">


									<form class="form-horizontal row-fluid" id="product_form" enctype="multipart/form-data" action="<?php echo URL::to('/product/update');?>" method="post">
                                    	<input type="hidden" name="prev" value="<?php echo @$_SERVER['HTTP_REFERER'];?>" />
                                    	<input type="hidden" name="id" value="<?php echo $product->id;?>">
										<div class="control-group">
											<label class="control-label" for="basicinput">Title</label>
											<div class="controls">
												<input type="text" id="title" name="title" placeholder="Title" value="<?php echo $product->product_name;?>" class="span8" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?> required>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" name="desc" id="desc" rows="5" placeholder="Description" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?>><?php echo $product->description;?></textarea>
											</div>
										</div>

										

										<div class="control-group">
											<label class="control-label" for="basicinput">Category</label>
											<div class="controls">
												<select tabindex="1" name="category" data-placeholder="Select here.." class="span8" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?>>
                                                <?php
													foreach ($categories as $row)
													{
														if ($product->cat_id==$row->id)
															echo "<option value='".$row->id."' selected>".$row->cat_name."</option>";
														else
															echo "<option value='".$row->id."'>".$row->cat_name."</option>";
													}
												?>
												</select>
											</div>
										</div>
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Price</label>
											<div class="controls">
												<input name="price" type="number"  min="500" max="999999999999" class="span8" id="price" placeholder="Price" size="20" value="<?php echo $product->price;?>" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?> required>
											</div>
										</div>
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Active</label>
											<div class="controls">
												<input type="checkbox" name="is_active" value="1" <?php if ($product->isActive=='1') echo 'checked="checked"';?> <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?> />
											</div>
										</div>
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Stock</label>
											<div class="controls">
												<select name="stock_type" id="stock_type" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?>>
                                                	<option value="1" <?php if ($product->stock_type=='1')echo' selected="selected"';?>>In Stock</option>
                                                	<option value="0" <?php if ($product->stock_type=='0')echo' selected="selected"';?>>Out of Stock</option>
                                                </select>
											</div>
										</div>
                                                                                
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Image 1</label>
											<div class="controls">
                                            	<?php if ($product->product_img_1!=''){?>
                                            	 <img src="<?php echo $imageURL."product_image/".$product->product_img_1;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="img_1" class="filevalidator" data-max-size='2m' data-type='image'>
											</div>
										</div>
                                        
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Image 2</label>
											<div class="controls">
                                           		<?php if ($product->product_img_2!=''){?>
                                            	 <img src="<?php echo $imageURL."product_image/".$product->product_img_2;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="img_2" class="filevalidator" data-max-size='2m' data-type='image'>
											</div>
										</div>
                                        
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Image 3</label>
											<div class="controls">
                                           		<?php if ($product->product_img_3!=''){?>
                                            	 <img src="<?php echo $imageURL."product_image/".$product->product_img_3;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="img_3" class="filevalidator" data-max-size='2m' data-type='image'>
											</div>
										</div>
                                        
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Image 4</label>
											<div class="controls">
                                           		<?php if ($product->product_img_4!=''){?>
                                            	 <img src="<?php echo $imageURL."product_image/".$product->product_img_4;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="img_4" class="filevalidator" data-max-size='2m' data-type='image'>
											</div>
										</div>

	
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn" <?php if ($privilege['access'] == 1) echo 'disabled="disabled"';?> id="buttonsubmit">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
@stop