@extends('ib')

@section('content')

        <script src="{{ URL::asset('scripts/jquery.validate.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/additional-methods.js') }}" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
				//$("#product_form").validate();
			});
        </script>
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Edit Category</h3>
							</div>
							<div class="module-body">


									<form class="form-horizontal row-fluid" id="product_form" enctype="multipart/form-data" action="<?php echo URL::to('/category/update');?>" method="post">
                                    	<input type="hidden" name="id" value="<?php echo $category->id;?>">
										<div class="control-group">
											<label class="control-label" for="basicinput">Category</label>
											<div class="controls">
												<input type="text" id="cat_name" name="cat_name" placeholder="Category Name" class="span8" value="<?php echo $category->cat_name;?>" required>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Category Indo</label>
											<div class="controls">
												<input type="text" id="cat_name_indo" name="cat_name_indo" placeholder="Category Name Indo" class="span8" value="<?php echo $category->cat_name_indo;?>" required>
											</div>
										</div>

										

										
                                        
										
                                                                                
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Image </label>
											<div class="controls">
                                            	<img src="<?php echo $publicURL."cat_pics/".$category->image;?>">
                                            	<input type="file" name="img_1" class="span8" >
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
@stop