@extends('ib')

@section('content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Offer</h3>
							</div>
							<div class="module-body">
								

								<div class="stream-list">
									<?php
										foreach ($offers as $row)
										{
											?>
                                            <div class="media stream">
										<a href="#" class="media-avatar medium pull-left">
                                        <?php if ($row->profile_pic!=''){ ?>
											<img src="<?php echo $imageURL."profile_image/".$row->profile_pic;?>">
                                       	<?php }else{ ?>
											<img src="{{ URL::asset('images/user.png') }}">
                                      	<?php } ?>
                                            
										</a>
										<div class="media-body">
											<div class="stream-headline">
												<h5 class="stream-author">
													<?php echo $row->first_name;?> <?php echo $row->last_name;?> 
													<small><?php echo date('F d,Y', strtotime($row->inserted_on));?></small>
												</h5>
												<div class="stream-text">
													 <?php echo $row->product_name;?> 
												
                                              
                                                
                                                
                                               

                                                
                                                </div>
												<div class="stream-attachment photo">
													<div class="responsive-photo">
														<img src="<?php echo $imageURL."product_image/".$row->product_img_1;?>" />
													</div>
												</div>
                                                <?php
												if (trim($row->product_img_2)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_2;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_3)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_3;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_4)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_4;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												?>
                                                <div class="stream-respond">
                                                <div class="media stream">
                                                            <a href="<?php echo URL::to('/user/show/'.$row->id_o) ?>" class="media-avatar small pull-left">
                                                            	<?php if ($row->profile_pic_o!=''){?>
                                                                <img src="<?php echo $imageURL."profile_image/".$row->profile_pic_o;?>">
                                                                <?php } else{?>
                                                                <img src="images/user.png">
                                                                <?php }?>
                                                            </a>
                                                            <div class="media-body">
                                                                <div class="stream-headline">
                                                                    <h5 class="stream-author">
                                                                        <?php echo $row->first_name_o;?> <?php echo $row->last_name_o;?> 
                                                                        <small><?php echo date('F d,Y', strtotime($row->offered_at));?></small>
                                                                    </h5>
                                                                    <div class="stream-text">
                                                                        <?php echo $row->offered_price;?>
                                                                    </div>
                                                                    <div class="stream-text">
                                                                        <a class="btn btn-small btn-primary confirm-offer" href="<?php echo URL::to('/offer/accept/'.$row->offer_id) ?>">Accept Offer</a>
                                                                    </div>
                                                                </div><!--/.stream-headline-->
                                                            </div>
                                                        </div>
                                               	</div>
											</div><!--/.stream-headline-->

											
										</div>
									</div>
                                            <?php
										}
									?>
									

									
									<div class="pagination pagination-centered">
                                                                            <ul>
                                                                                <li><a href="<?php echo ($page>1)?URL::to('/offer?page='.($page-1)):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                                                                <li><a href="<?php echo URL::to('/offer?page='.($page+1));?>"><i class="icon-double-angle-right"></i></a></li>
                                                                            </ul>
                                                                        </div>
								</div><!--/.stream-list-->
							</div><!--/.module-body-->
						</div><!--/.module-->
						
					</div><!--/.content-->
				</div>
                <script type="text/javascript">
				$(document).ready(function () {
					$(".confirm-offer").click(function(event){
                    
                    	var href = $(this).prop('href');
						event.preventDefault();
						confirmAction("Are you sure you want to accept this offer?", function () {
                            window.location.href = href;
                        });
					});
				});
				</script>
@stop