@extends('ib')

@section('content')
<div class="span9">
    <div class="content">

        <div class="module">
            <div class="module-head">
                <h3>Category</h3>
            </div>
            <div class="module-body">
            
			<?php if ($privilege['access']==2){?>
            <p><a class="btn btn-small btn-primary" href="<?php echo URL::to('/category/create/') ?>">Add New</a></p>
            <?php } ?>
            <div class="top-row">
            		<?php foreach ($categories as $row){?>
                    <div class="col-xs-4" style="padding-left: 100px;">
						<div class="top-row-col1 text-center">
							<h2><?php echo $row->cat_name;?></h2>
							<img class="r-img text-center" src="<?php echo $publicURL."cat_pics/".$row->image;?>" title="name">
							<h4>TOTAL</h4>
							<label><?php echo $row->total;?> PRODUCTS</label>
                            
							<?php if ($privilege['access']==2){?>
                            <a href="<?php echo URL::to('/category/edit/'.$row->id) ?>" class="btn btn-small btn-info">Edit</a>
                            <?php if ($row->total==0){?>
                            <a href="<?php echo URL::to('/category/delete/'.$row->id) ?>" class="btn btn-small btn-danger confirm-delete">Delete</a>
                            <?php } ?>
                            <?php } ?>
						</div>
					</div>
                    <?php } ?>
					<vdi class="clearfix"> </vdi></div>
            
            
            </div>
       	</div><!--/.module-->
        
    </div><!--/.content-->
</div>

                <script type="text/javascript">
				$(document).ready(function () {
					$(".confirm-delete").click(function(event){
                    
                    	var href = $(this).prop('href');
						event.preventDefault();
						confirmAction("Are you sure you want to delete the selected category?", function () {
                            window.location.href = href;
                        });
					});
				});
				</script>
@stop