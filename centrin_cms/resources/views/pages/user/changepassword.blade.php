@extends('ib')

@section('content')
<div class="span9">
<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Change Password</h3>
							</div>
							<div class="module-body">
                            <?php if ($status=='1'){?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Password has been updated successfully!</strong>
                            </div>
                            <?php } ?>
                            <form class="form-horizontal row-fluid" id="user_form" enctype="multipart/form-data" action="<?php echo URL::to('/profile/update_password');?>" method="post">
                            	<input type="hidden" id='email' name="email" value="<?php echo Session::get('email');?>" />
                                <div class="control-group">
											<label class="control-label" for="basicinput">Old Password</label>
											<div class="controls">
												<input type="password" id="old_password" name="old_password" placeholder="Old Password"   class="span8"  required>
											</div>
										</div>
                            	<div class="control-group">
											<label class="control-label" for="basicinput">New Password</label>
											<div class="controls">
												<input type="password" id="password" name="password" placeholder="New Password"  class="span8" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$" minlength=6 title="Minimum 6 characters at least 1 Alphabet and 1 Number" required>
											</div>
										</div>
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Confirm Password</label>
											<div class="controls">
												<input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password"  class="span8" required>
											</div>
										</div>
                                        
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit</button>
											</div>
										</div>
                            </form>
                            </div>
                      	</div>
</div>
</div>
<script>


				$('#user_form').submit(function(event){
					
					//alert('here')
					var email = $('#email').val();
					var old_password = $('#old_password').val();
					var returnValue=false;
					$.ajax({
							type: 'get',
							start: function() { },
							complete: function() {  },
							url: '<?php echo URL::to('/user/check_password');?>?email='+email+'&old_password='+old_password,
							dataType: 'json',
							async: false,
							cache: false,
							error: function() {
								alert('Error!')
							},
							success: function(msg) {
								status=msg.status;
								if (status==1)
								{
									returnValue =  true;
								}else
								{
									alert('Old Password didn\'t match');
								}
							}
					});
					return returnValue;
				});

var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
@stop