@extends('ib')

@section('content')

        <link type="text/css" href="{{ URL::asset('css/jquery.ui.timepicker.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      	
        <script src="{{ URL::asset('scripts/jquery.ui.timepicker.js') }}" type="text/javascript"></script>
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Create User</h3>
							</div>
							<div class="module-body">
                            		<?php if ($status=='1'){?>
                            		<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>User created successfully!</strong>
									</div>
                                    <?php } ?>


									<form class="form-horizontal row-fluid" id="user_form" enctype="multipart/form-data" action="<?php echo URL::to('/user/store');?>" method="post">
										<div class="control-group">
											<label class="control-label" for="basicinput">Email</label>
                                            <div class="controls">
												<input type="email" id="email" name="email" placeholder="Email"  class="span8" required>
											</div>
											
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Store Name</label>
											<div class="controls">
												<input type="text" id="username" name="username" placeholder="Store Name"  class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">First Name</label>
											<div class="controls">
												<input type="text" id="first_name" name="first_name" placeholder="First Name"  class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Last Name</label>
											<div class="controls">
												<input type="text" id="last_name" name="last_name" placeholder="Last Name"  class="span8" value="" >
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Role</label>
											<div class="controls">
												<select name="user_role">
                                                <?php
													foreach ($groups as $row)
													{
														if (($row->id!=1)||(Session::get('group_id')==1))
															echo "<option value='".$row->id."'>".$row->name."</option>";
													}
												?>
                                                </select>
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Opening Hour</label>
											<div class="controls">
												<input name="opening_hour_start" placeholder="Opening hour" class="time_selector" style="width:100px;" value="" required/>
											</div>
										</div>
                                        
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Closing Hour</label>
											<div class="controls">
												<input name="opening_hour_end"  class="time_selector" style="width:100px;" value="" placeholder="Closing hour" required/>
											</div>
										</div>
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Shipping Fee</label>
											<div class="controls">
												<input type="number" id="shipping_fee" name="shipping_fee" placeholder="Shipping fee" value=""  class="span8">
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Mobile</label>
											<div class="controls">
												<input name="mobile" type="text" class="span8" id="mobile" placeholder="Mobile" value="" size="20" required>
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Bio</label>
											<div class="controls">
												<textarea name="bio" id="bio" placeholder="Bio"></textarea>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label" for="basicinput">Profile Pic</label>
                                            <div class="controls">
                                            	<input type="file" name="profile_pic" class="span8">
											</div>
										</div>
                                        

										
	
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit Form</button>
											</div>
										</div>
									</form>
									<br>                                  
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
                
    
				<script type="text/javascript">
				/*$('.time_selector').timepicki({
					show_meridian:false,
					min_hour_value:0,
					max_hour_value:23,
					step_size_minutes:5,
					disable_keyboard_mobile: false,
					disable_keyboard: false
				});*/
				$('.time_selector').timepicker();
				$('#user_form').submit(function(event){
					
					//alert('here')
					var email = $('#email').val();
					var returnValue=false;
					$.ajax({
							type: 'get',
							start: function() { },
							complete: function() {  },
							url: '<?php echo URL::to('/user/check');?>?email='+email,
							dataType: 'json',
							async: false,
							cache: false,
							error: function() {
								alert('Error!')
							},
							success: function(msg) {
								status=msg.status;
								if (status==1)
								{
									alert('Email is already registered!');
								}else
								{
									returnValue=  true;
								}
							}
					});
					return returnValue;
				});
                </script>
@stop