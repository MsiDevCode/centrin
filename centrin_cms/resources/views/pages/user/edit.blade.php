@extends('ib')

@section('content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Edit Profile</h3>
							</div>
							<div class="module-body">


									<form class="form-horizontal row-fluid" id="product_form" enctype="multipart/form-data" action="<?php echo URL::to('/user/update');?>" method="post">
										<div class="control-group">
											<label class="control-label" for="basicinput">Username</label>
                                            <div class="controls">
												<input type="text" id="user_name" name="user_name" placeholder="User Name" value="<?php echo $user->user_name;?>" class="span8" disabled>
											</div>
											
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">First Name</label>
											<div class="controls">
												<input type="text" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $user->first_name;?>" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Last Name</label>
											<div class="controls">
												<input type="text" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $user->last_name;?>" class="span8" required>
											</div>
										</div>
										
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Mobile</label>
											<div class="controls">
												<input name="mobile" type="text" class="span8" id="mobile" placeholder="Mobile" value="<?php echo $user->mobile;?>" size="20" required>
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Bio</label>
											<div class="controls">
												<textarea name="bio" id="bio"></textarea>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label" for="basicinput">Profile Pic</label>
											<div class="controls">
                                            	<?php if ($user->profile_pic!=''){?>
                                            	 <img src="<?php echo $imageURL."profile_image/".$user->profile_pic;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="profile_pic" class="span8">
											</div>
										</div>
                                        

										
	
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit Form</button>
											</div>
										</div>
									</form>
									<br>
									<br>
									<div class="module-head">
										<h3>Edit Bank Account</h3>
									</div>
									<form class="form-horizontal row-fluid" id="bank_form" enctype="multipart/form-data" action="<?php echo URL::to('/user/update_bank');?>" method="post">
									
									
									<div class="control-group">
											<label class="control-label" for="basicinput">Bank </label>
											<div class="controls">
												<select tabindex="1" name="bank" data-placeholder="Select here.." class="span8">
                                                <?php
													foreach ($banks as $row)
													{
														/*
														if ($product->cat_id==$row->id)
															echo "<option value='".$row->id."' selected>".$row->cat_name."</option>";
														else*/
															echo "<option value='".$row->id."'>".$row->bank_name."</option>";
													}
												?>
												</select>
											</div>
										</div>
									</form>                                    
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
@stop