@extends('ib')

@section('content')
<div class="span9">
                    <div class="content">
                   	<form method="post" action="<?php echo URL::to('/inactive_user/submit/');?>">
                        <div class="module">
                            <div class="module-head">
                                <h3>
                                    Inactive User</h3>
                            </div>
                            <div class="module-option clearfix">
                                <div class="input-append pull-left">
                                	<label>Update Selected</label>
                                    <select name="status">
                                    	<option value="0">Inactive</option>
                                    	<option value="1">Active</option>
                                    </select>
                                    <button type="submit" class="btn" >
                                        Submit
                                    </button>
                                    <input type="hidden" name="asd" value="asd">
                                </div>
                                <div class="input-append pull-right">
                                	<label id="selectall"><a>Select/Unselect All</a></label>
                                </div>
                                	
                                
                            </div>
                            <div class="module-body">
                            
                            	<?php
									$counter=0;
									foreach ($users as $row)
									{
										if ($counter%2==0)
											echo '<div class="row-fluid">';
										?>
                                        
                                        
                                            <div class="span6">
                                                <div class="media user">
                                                    <a class="media-avatar pull-left" href="<?php echo URL::to('/user/show/'.$row->id) ?>" <?php if ($row->is_active=='0'){?>style="background-color:#FCC;"<?php } ?>>
                                                    	<?php if (trim($row->profile_pic)!='') {?>
                                                          <img src="<?php echo $imageURL."profile_image/".$row->profile_pic;?>">
                                                      	<?php }else {?>
                                                          <img src="images/user.png">
                                                        <?php } ?>
                                                    </a>
                                                    <div class="media-body">
                                                    	<input type="checkbox" class="selector" name="users[]" value="<?php echo $row->id;?>">
                                                        <h3 class="media-title">
                                                            <?php echo $row->first_name." ".$row->last_name;?>
                                                        </h3>
                                                        <p>
                                                            <small class="muted"><?php echo $row->email;?></small>
                                                            </p>
                                                        <div class="media-option btn-group shaded-icon">
                                                            <button class="btn btn-small">
                                                                <i class="icon-envelope"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        
                                        <?php
										if ($counter%2==1)
											echo '</div>';
										$counter++;
									}
								?>
                            	
                                <div class="pagination pagination-centered">
                                    <ul>
                                        <li><a href="<?php echo ($page>1)?URL::to('/inactive_user?page='.($page-1)):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                        <li><a href="<?php echo URL::to('/inactive_user?page='.($page+1));?>"><i class="icon-double-angle-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.content-->
                    </form>
                </div>
                
                <script type="text/javascript">
				
				var selectAll = false;
				$(document).ready(function () {
					$("#filter").click(function(){
						event.preventDefault();
						value=$("#filter_text").val();
						window.location.replace('<?php echo URL::to('/inactive_user');?>'+'?name='+value);
					});
					
					$("#selectall").click(function(){
						if (!selectAll)
							selectAll=true;
						else
							selectAll=false;
							
						$( ".selector" ).each(function( index ) {
							if (selectAll)
						  		$(this).prop('checked', true);
							else
								$(this).prop('checked', false);
						});
					});
				});
				</script>
@stop