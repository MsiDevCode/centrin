@extends('ib')

@section('content')
<div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-body">
                                <div class="profile-head media">
                                    <a href="#" class="media-avatar pull-left">
                                        <img src="{{ URL::asset('images/user.png') }}">
                                    </a>
                                    <div class="media-body">
                                        <h4>
                                            <?php echo $detail->first_name." ".$detail->last_name;?> <small><?php echo "(Last login : ".$detail->last_login.")";?></small>
                                        </h4>
                                        <p class="profile-brief">
                                            <?php echo $detail->email;?>
                                        </p>
                                        
                                        <div class="profile-details muted">
                                            <a href="#" class="btn">Total Following : <?php echo $detail->total_following;?> </a>
                                            <a href="#" class="btn">Total Follower : <?php echo $detail->total_follower;?> </a>
                                        </div>
                                        
                                    </div>
                                </div>
                                <ul class="profile-tab nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab">Products</a></li>
                                    <li><a href="#friends" data-toggle="tab">Friends</a></li>
                                </ul>
                                <div class="profile-tab-content tab-content">
                                    <div class="tab-pane fade active in" id="activity">
                                        <div class="stream-list">
									<?php
										foreach ($products as $row)
										{
											?>
                                            <div class="media stream">
										<a href="#" class="media-avatar medium pull-left">
											<img src="<?php echo $imageURL."profile_image/".$row->profile_pic;?>">
										</a>
										<div class="media-body">
											<div class="stream-headline">
												<h5 class="stream-author">
													<?php echo $row->first_name;?> <?php echo $row->last_name;?> 
													<small><?php echo date('F d,Y', strtotime($row->inserted_on));?></small>
												</h5>
												<div class="stream-text">
													 <?php echo $row->product_name;?> 
												
                                              
                                                
                                                
                                               

                                                
                                                </div>
												<div class="stream-attachment photo">
													<div class="responsive-photo">
														<img src="<?php echo $imageURL."product_image/".$row->product_img_1;?>" />
													</div>
												</div>
                                                <?php
												if (trim($row->product_img_2)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_2;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_3)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_3;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_4)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_4;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (count($row->comments)>0)
												{
													?>
                                                    <div class="stream-respond">
                                                    <?php
													foreach ($row->comments as $rowC)
													{?>
                                                        <div class="media stream">
                                                            <a href="#" class="media-avatar small pull-left">
                                                                <img src="<?php echo $imageURL."profile_image/".$rowC->profile_pic;?>">
                                                            </a>
                                                            <div class="media-body">
                                                                <div class="stream-headline">
                                                                    <h5 class="stream-author">
                                                                        <?php echo $rowC->first_name;?> <?php echo $rowC->last_name;?> 
                                                                        <small><?php echo date('F d,Y', strtotime($rowC->commented_on));?></small>
                                                                    </h5>
                                                                    <div class="stream-text">
                                                                        <?php echo $rowC->comment_text;?>
                                                                    </div>
                                                                </div><!--/.stream-headline-->
                                                            </div>
                                                        </div><!--/.media .stream-->
                                                   	<?php } ?>
												
											</div>
                                                    <?php
												}
												?>
                                                
											</div><!--/.stream-headline-->

											
										</div>
									</div>
                                            <?php
										}
									?>
									

									<div class="media stream load-more">
										<a href="#" id="show_more">
											<i class="icon-refresh shaded"></i>
											show more...
										</a>
									</div>
								</div>
                                        <!--/.stream-list-->
                                    </div>
                                    <div class="tab-pane fade" id="friends">
                                        <div class="module-option clearfix">
                                            <form>
                                            <div class="input-append pull-left">
                                                <input type="text" class="span3" placeholder="Filter by name...">
                                                <button type="submit" class="btn">
                                                    <i class="icon-search"></i>
                                                </button>
                                            </div>
                                            </form>
                                            <div class="btn-group pull-right" data-toggle="buttons-radio">
                                                <button type="button" class="btn">
                                                    All</button>
                                                <button type="button" class="btn">
                                                    Male</button>
                                                <button type="button" class="btn">
                                                    Female</button>
                                            </div>
                                        </div>
                                        <div class="module-body">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                John Donga
                                                            </h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                Donga John</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.row-fluid-->
                                            <br />
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                John Donga</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                Donga John</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.row-fluid-->
                                            <br />
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                John Donga</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                Donga John</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.row-fluid-->
                                            <br />
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                John Donga</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="media user">
                                                        <a class="media-avatar pull-left" href="#">
                                                            <img src="{{ URL::asset('images/user.png') }}">
                                                        </a>
                                                        <div class="media-body">
                                                            <h3 class="media-title">
                                                                Donga John</h3>
                                                            <p>
                                                                <small class="muted">Pakistan</small></p>
                                                            <div class="media-option btn-group shaded-icon">
                                                                <button class="btn btn-small">
                                                                    <i class="icon-envelope"></i>
                                                                </button>
                                                                <button class="btn btn-small">
                                                                    <i class="icon-share-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.row-fluid-->
                                            <br />
                                            <div class="pagination pagination-centered">
                                                <ul>
                                                    <li><a href="#"><i class="icon-double-angle-left"></i></a></li>
                                                    <li><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#"><i class="icon-double-angle-right"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.module-body-->
                        </div>
                        <!--/.module-->
                    </div>
                    <!--/.content-->
                </div>
@stop