<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Instabeli</title>
        <link type="text/css" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/theme.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('images/icons/css/font-awesome.css') }}" rel="stylesheet">
      	
        <script src="{{ URL::asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/flot/jquery.flot.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
					<i class="icon-reorder shaded"></i>
				</a>

		  	  <a class="brand" href="#">
			  		Instabeli
			  	</a>

				<div class="nav-collapse collapse navbar-inverse-collapse"> </div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->



	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="module module-login span4 offset4">
                	<?php 
						$error = Session::get('error');
						if ($error!='')
						{?>
						<div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo $error;?> 
                        </div>
                        <?php	
						}
					?>
                    <?php 
						$success = Session::get('success');
						if ($success!='')
						{?>
						<div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo $success;?> 
                        </div>
                        <?php	
						}
					?>
					<form class="form-vertical" method="post" action="<?php echo URL::to('/login/reset') ?>">
						<div class="module-head">
							<h3>Forget Password</h3>
						</div>
						<div class="module-body">
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="text" id="inputEmail" placeholder="Email" name="email">
								</div>
							</div>
                            <div class="control-group">
								<div class="controls row-fluid">
									<a href="<?php echo URL::to('/login') ?>"> Login Page</a>
								</div>
							</div>
						</div>
                            
						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="submit" class="btn btn-primary pull-right">Submit</button>
									
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!--/.wrapper-->

	<div class="footer">
		<div class="container">
			 

			<b class="copyright">&copy; 2015 Instabeli </b> All rights reserved.
		</div>
	</div>
</body>