@extends('ib')

@section('content')


        <link type="text/css" href="{{ URL::asset('css/jquery.ui.timepicker.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      	
        <script src="{{ URL::asset('scripts/jquery.ui.timepicker.js') }}" type="text/javascript"></script>
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Edit Profile</h3>
							</div>
							<div class="module-body">
                            		<?php if ($status=='1'){?>
                            		<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>User updated successfully!</strong>
									</div>
                                    <?php } ?>


									<form class="form-horizontal row-fluid" id="product_form" enctype="multipart/form-data" action="<?php echo URL::to('/user/update');?>" method="post">
                                    <input type="hidden" name="id" value="<?php echo $user->id;?>" />
										<div class="control-group">
											<label class="control-label" for="basicinput">Email</label>
                                            <div class="controls">
												<input type="text" id="email" name="email" placeholder="Email" value="<?php echo $user->email;?>" class="span8" disabled>
											</div>
											
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Store Name</label>
											<div class="controls">
												<input type="text" id="username" name="username" placeholder="Store Name" value="<?php echo $user->user_name;?>" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">First Name</label>
											<div class="controls">
												<input type="text" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $user->first_name;?>" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Last Name</label>
											<div class="controls">
												<input type="text" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $user->last_name;?>" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Role</label>
											<div class="controls">
												<select name="user_role">
                                                <?php
													foreach ($groups as $row)
													{
														foreach ($privileges as $rowP)
														{
															if (($row->id!=1)||(Session::get('group_id')==1))
															{
																if (@$rowP->group_id==$row->id)
																	echo "<option value='".$row->id."' selected='selected'>".$row->name."</option>";
																else
																	echo "<option value='".$row->id."'>".$row->name."</option>";
															}
														}
														
													}
												?>
                                                </select>
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Opening Hour</label>
											<div class="controls">
												<input name="opening_hour_start" class="time_selector" style="width:100px;" value="<?php echo $user->opening_hour_start;?>" required/>
											</div>
										</div>
                                        
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Closing Hour</label>
											<div class="controls">
												<input name="opening_hour_end"  class="time_selector" style="width:100px;" value="<?php echo $user->opening_hour_end;?>" required/>
											</div>
										</div>
                                        
										<div class="control-group">
											<label class="control-label" for="basicinput">Shipping Fee</label>
											<div class="controls">
												<input type="text" id="shipping_fee" name="shipping_fee" placeholder="Shipping fee" value="<?php echo $user->shipping_fee;?>"  class="span8">
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Mobile</label>
											<div class="controls">
												<input name="mobile" type="text" class="span8" id="mobile" placeholder="Mobile" value="<?php echo $user->mobile;?>" size="20" >
											</div>
										</div>
                                        
                                        <div class="control-group">
											<label class="control-label" for="basicinput">Bio</label>
											<div class="controls">
												<textarea name="bio" id="bio"></textarea>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label" for="basicinput">Profile Pic</label>
											<div class="controls">
                                            	<?php if ($user->profile_pic!=''){?>
                                            	 <img src="<?php echo $imageURL."profile_image/".$user->profile_pic;?>"><br><br>
                                             	<?php } ?>
                                            	<input type="file" name="profile_pic" class="span8">
											</div>
										</div>
                                        

										
	
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit Form</button>
											</div>
										</div>
									</form>
									<br>                                  
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
                
    
				<script type="text/javascript">
				/*$('.time_selector').timepicki({
					show_meridian:false,
					min_hour_value:0,
					max_hour_value:23,
					step_size_minutes:5,
					disable_keyboard_mobile: false,
					disable_keyboard: false
				});*/
				$('.time_selector').timepicker();
                </script>
@stop