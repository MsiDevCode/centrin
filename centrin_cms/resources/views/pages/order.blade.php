@extends('ib')

@section('content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Order</h3>
							</div>
							<div class="module-body">
								<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
                                <div class="module-option clearfix">
                                <form>
                                <div class="input-append pull-left">
                                	<select name="order_status" id="order_status" style="width:100px;">
                                    	<option value="0">all</option>
                                    	<?php
											foreach ($status as $row)
											{
												if ($row->id==$orderStatus)
												{
													echo "<option value='".$row->id."'  selected='selected'>".$row->name."</option>";
												}else
												{
													echo "<option value='".$row->id."' >".$row->name."</option>";
												}
											}
										?>
                                    </select>
                                    <input type="text" class="span3" value="<?php echo $orderNumber;?>" placeholder="Filter by order number..." id="filter_text">
                                    <input type="text" id="from" name="from" style="width:100px" value="<?php echo $from;?>" placeholder="Start date..." />
                                    <input type="text" id="to" name="to" style="width:100px" value="<?php echo $to;?>" placeholder="End date..." />
                                    
                                    <button type="submit" class="btn" id="filter">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                                </form>
                                
                            </div>
                            <div class="module-body table">
                                                                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                                                    width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                No
                                                                            </th>
                                                                            <th>
                                                                                Customer Name
                                                                            </th>
                                                                            <th>
                                                                                Order Number
                                                                            </th>
                                                                            <th>
                                                                                Grand Total
                                                                            </th>
                                                                            <th>
                                                                                Order Date
                                                                            </th>
                                                                            <th>
                                                                                Status
                                                                            </th>
                                                                            <th>&nbsp;
                                                                                
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php $counter = $start+1; foreach ($orders as $row){?>
                                                                    	<tr class="odd gradeX">
                                                                        	<td><?php echo $counter;?></td>
                                                                        	<td><?php echo $row->customer_name;?></td>
                                                                        	<td><?php echo $row->order_number;?></td>
                                                                        	<td align="right" valign="middle">Rp <?php echo number_format($row->grand_total);?></td>
                                                                        	<td><?php echo $row->created_at;?></td>
                                                                        	<td><b class="<?php echo $row->order_status;?>-status"><?php echo $row->order_status;?></b></td>
                                                                            <td><a class="btn-box small" href="<?php echo URL::to('/order/detail/'.$row->id) ?>"><i class="icon-edit"></i></a>&nbsp;
                                                                            <!--<a class="btn-box small" href="<?php echo URL::to('/order/print/'.$row->id) ?>"><i class="icon-print"></i></a>-->
                                                                            <?php
																				if (($row->status_id=='1')&&($privilege['access']==2)){?>
                                                                                <a href="#" class="btn-box small cancel" order_id=<?php echo $row->id;?>"><i class="icon-remove"></i></a>
                                                                            <?php
																				}
																			?>
                                                                            </td>

                                                                        </tr>
                                                                    <?php $counter++; } ?>
                                                                    <?php
																		if (count($orders)==0){
																	?>
                                                                    <div class="alert">
                                                                        <button type="button" class="close" data-dismiss="alert">×</button> No Order found!
                                                                    </div>
                                                                    <?php
																		}
																	?>
                                                                    </tbody>
                                                                    
                                                                </table>
                                                            </div>
								
							</div><!--/.module-body-->
                            <div class="pagination pagination-centered">
                                    <ul>
                                        <li><a href="<?php echo ($page>1)?URL::to('/order?order_status='.$orderStatus.'&order_number='.$orderNumber.'&page='.($page-1).'&from='.$from.'&to='.$to):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                        <li><a href="<?php echo URL::to('/order?order_status='.$orderStatus.'&order_number='.$orderNumber.'&page='.($page+1).'&from='.$from.'&to='.$to);?>"><i class="icon-double-angle-right"></i></a></li>
                                    </ul>
                                </div>
						</div><!--/.module-->
						
					</div><!--/.content-->
				</div>
                
<div id='repost-form' style="display:none">
                <div class='header-modal'><span>Product Repost </span></div>
                <form action="<?php echo URL::to('/product/repost/') ?>" method="post" id="product_form">
                <input type="hidden" name="product_id" id="product_id" value=""/>
                <div class='message'>Quantity &nbsp;:&nbsp; <input type="text" name="total" value="" style="height:30px;" required></div>
                <input type="submit" name="submit" value="submit" style="  margin-left: 10px;" />
                </form>
                
                <div id='cancel-form' style="display:none">
                <div class='header-modal'><span>Order Cancel </span></div>
                <form action="<?php echo URL::to('/order/cancel/') ?>" method="post" id="product_form">
                <input type="hidden" name="order_id" id="order_id" value=""/>
                <div class='message'>Reason &nbsp;:&nbsp; <textarea name="reason" required></textarea></div>
                <input type="submit" name="submit" value="submit" style="  margin-left: 10px;" />
                </form>
            </div>
            </div>
            <script>
				$(document).ready(function () {
                	
                    $(function() {
    $( "#from" ).datepicker({
      dateFormat : 'yy-mm-dd',
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      changeMonth: true,
      dateFormat : 'yy-mm-dd',
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
                    
					$("#filter").click(function(){
						event.preventDefault();
						order_number=$("#filter_text").val();
						order_status=$("#order_status").val();
						from=$("#from").val();
						to=$("#to").val();
						window.location.replace('<?php echo URL::to('/order');?>'+'?order_number='+order_number+'&order_status='+order_status+'&from='+from+'&to='+to);
					});
                    
					$(".cancel").click(function(event){
						var order_id = $(this).attr('order_id');
						$('#order_id').val(order_id);
						//event.preventDefault();
						$('#cancel-form').modal({
							closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
							position: ["20%",],
							overlayId: 'confirm-overlay',
							containerId: 'confirm-container', 
							onShow: function (dialog) {
								var modal = this;
								/*
								$('.message', dialog.data[0]).append(message);
					
								// if the user clicks "yes"
								$('.yes', dialog.data[0]).click(function () {
									// call the callback
									if ($.isFunction(callback)) {
										callback.apply();
									}
									// close the dialog
									modal.close(); // or $.modal.close();
								});
								*/
							}
						});
						
					});
				});
			</script>
                
                
@stop
