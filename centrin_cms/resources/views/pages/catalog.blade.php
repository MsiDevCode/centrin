@extends('ib')

@section('content')

                                <form id="multiple_submit" action="<?php echo URL::to('/catalog/submit/');?>" method="post">
<div class="span9">
    <div class="content">

        <div class="module">
        
        <div class="module-head">
								<h3>Product</h3>
							</div>
		<div class="module-body">
								<div class="module-option clearfix">
                                <div class="input-append pull-left">
                                    <input type="text" class="span3" placeholder="Filter by name..." id="filter_text" value="<?php echo $name;?>">
                                    <select name="category_id" id="category_id" class="span3">
                                    	<option value="">-Select Category-</option>
                                        <?php
											foreach ($categories as $row)
											{
												if ($row->id==$categoryId)
													echo "<option value=\"".$row->id."\"  selected='selected'>".$row->cat_name."</option>";
												else
													echo "<option value=\"".$row->id."\">".$row->cat_name."</option>";
											}
										?>
                                    </select>
                                    <select name="status" id="status" class="span3" style="width:200px;">
                                    	<option value="">-Select Status-</option>
                                    	<option value="1" <?php if ($status=='1') echo 'selected="selected"';?>>Active</option>
                                    	<option value="0" <?php if ($status=='0') echo 'selected="selected"';?>>Inactive</option>
                                    </select>
                                    <button type="submit" class="btn" id="filter">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                                
                            </div>
                                
                            
        					
                            
        <div class="special-products-grids" style="border-top: 1px solid #e5e5e5;">
        						<p>&nbsp;</p>
        						<?php if ($privilege['access']==2){?>
                                <div class="input-append pull-left">
                                <a class="btn btn-small btn-primary" href="<?php echo URL::to('/product/create/') ?>">Add New</a>
                                </div>
                                <?php } ?>
                            
        <p>&nbsp;</p>
        <?php
																		if (count($products)==0){
																	?>
                                                                    <div class="alert">
                                                                        <button type="button" class="close" data-dismiss="alert">×</button> No Product found!
                                                                    </div>
                                                                    <?php
																		}
																	?>
        <?php
		foreach ($products as $row)
		{?>
        <div class="col-md-3 special-products-grid text-center" <?php if ($row->isActive=='0') echo 'style="background-color:#FCC;"';?>>
        	<input type="checkbox" class="selector" name="products[]" value="<?php echo $row->id;?>" style="height:15px;width: 15px;"><br />
        	<?php if ($privilege['access']==2){?>
            <a class="product-here" href="<?php echo URL::to('/product/edit/'.$row->id) ?>"><img class="product-list" src="<?php echo $imageURL."product_image/thumb/".$row->product_img_1;?>" title="product-name"></a>
            <?php }else{?>
            <img class="product-list" src="<?php echo $imageURL."product_image/thumb/".$row->product_img_1;?>" title="product-name">
            <?php }?>
            <h4><a href="<?php echo URL::to('/product/edit/'.$row->id) ?>"><?php echo $row->product_name;?></a></h4>
            <h4>(<?php  echo (($row->is_blocked==0)? $row->category : "<b style='color:red'>Blocked</b>");?>)</h4>
            <h4>Rp <?php echo number_format($row->price);?></h4>
            
        </div>
        <?php } ?>
						
						<div class="clearfix" style="border-bottom: 1px solid #e5e5e5;">&nbsp; </div>
        
                                <div class="input-append pull-left">
                                	<label id="selectall"><a>Select/Unselect All</a></label>
                                </div>
            
                                <div class="input-append pull-right">
                                	<label>Update Selected</label>
                                    <select name="status" id="list_status">
                                    	<option value="0">Inactive</option>
                                    	<option value="1">Active</option>
                                    	<option value="2">Move to</option>
                                        <?php if ($privilege['data']==1){?>
										<option value="-1">Block</option>
                                        <option value="-2">Unblock</option>
										<?php } ?>
                                    </select>
                                    <select name="new_category" id="new_category" style="display:none">
                                    	<?php
											foreach ($categories as $row)
											{
												echo "<option value='".$row->id."'>".$row->cat_name."</option>";
											}
										?>
                                    </select>
                                    <button type="submit" class="btn" >
                                        Submit
                                    </button>
                                    <input type="hidden" name="asd" value="asd">
                                </div>
                                <div class="clearfix"> </div>
        </div><!--/.module-->
        
                                
		
     	
        <p></p>
        <div class="pagination pagination-centered">
                                    <ul>
                                        <li><a href="<?php echo ($page>1)?URL::to('/catalog?page='.($page-1)."&name=$name&category_id=$categoryId&status=$status"):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                        <li><a href="<?php echo URL::to('/catalog?page='.($page+1)."&name=$name&category_id=$categoryId&status=$status");?>"><i class="icon-double-angle-right"></i></a></li>
                                    </ul>
                                </div>
    	</div>
		</div>
        </div>
</div>
    
                                </form>            

  
                              
                
                <script type="text/javascript">
				
				var selectAll = false;
				$(document).ready(function () {
					$("#filter").click(function(){
						event.preventDefault();
						value=$("#filter_text").val();
						category=$("#category_id").val();
						status=$("#status").val();
						//alert(status);
						window.location.replace('<?php echo URL::to('/catalog');?>'+'?name='+value+'&category_id='+category+'&status='+status);
					});
					
					$( "#list_status" ).change(function() {
						value = $(this).val();
						if (value==2)
							$('#new_category').show('slow');
						else
							$('#new_category').hide('slow');
					});
					
					$("#selectall").click(function(){
						if (!selectAll)
							selectAll=true;
						else
							selectAll=false;
							
						$( ".selector" ).each(function( index ) {
							if (selectAll)
						  		$(this).prop('checked', true);
							else
								$(this).prop('checked', false);
						});
					});
					
					$('#multiple_submit').submit(function(event){
						var total = 0;
						$( ".selector" ).each(function() {
						  if ($( this ).prop('checked'))
						  {
						  	total = total +1;
						  }
						});
						//alert(total);
						if (total==0)
						{
							alert('Please select at least 1 item!');
							return false;
						}else
						{
							return true;
						}
						
					});
				});
				</script>
@stop
