@extends('ib')

@section('content')
<div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head">
                                <h3>
                                    All Members</h3>
                            </div>
                            <div class="module-option clearfix">
                                <form>
                                <div class="input-append pull-left">
                                    <input type="text" class="span3" placeholder="Filter by name..." id="filter_text">
                                    <button type="submit" class="btn" id="filter">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                                </form>
                                
                                
                                
                            </div>
                            <?php if ($privilege['access']==2){?>
                            	
                            	<div class="module-option clearfix">
                                <div class="input-append pull-left">
                                <a class="btn btn-small btn-primary" href="<?php echo URL::to('/user/create/') ?>">Add New</a>
                                </div>
                                </div>
                                <?php } ?>
        						
                            <div class="module-body" style="border-top: 1px solid #e5e5e5;">
                            
                            	<?php
									$counter=0;
									foreach ($users as $row)
									{
										if ($counter%2==0)
											echo '<div class="row-fluid">';
										?>
                                        
                                        
                                            <div class="span6">
                                                <div class="media user">
                                                	
                                                    <a class="media-avatar pull-left" href="<?php  echo ($privilege['access']==2)?URL::to('/user/edit/'.$row->id):'#'; ?>">
                                                    	<?php if (trim($row->profile_pic)!='') {?>
                                                          <img src="<?php echo $imageURL."profile_image/".$row->profile_pic;?>">
                                                      	<?php }else {?>
                                                          <img src="images/user.png">
                                                        <?php } ?>
                                                   	
                                                    </a>
                                                    <div class="media-body">
                                                        <h3 class="media-title">
                                                            <?php echo $row->first_name." ".$row->last_name;?>
                                                        </h3>
                                                        <p>
                                                            <small class="muted"><?php echo $row->email;?></small></p>
                                                        
                                                        <div class="media-option btn-group shaded-icon">
                                                        	<a href="<?php echo URL::to('/user/reset/'.$row->id) ?>" title="Reset Password">
                                                            <button class="btn btn-small" >
                                                                <i class="icon-random"></i>
                                                            </button>
                                                            </a>
                                                            <!--
                                                            <button class="btn btn-small">
                                                                <i class="icon-share-alt"></i>
                                                            </button>
                                                            -->
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        
                                        <?php
										if ($counter%2==1)
											echo '</div>';
										$counter++;
									}
								?>
                            	
                                
                            </div>
                        </div>
                        <div class="pagination pagination-centered">
                                    <ul>
                                        <li><a href="<?php echo ($page>1)?URL::to('/user?page='.($page-1)):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                        <li><a href="<?php echo URL::to('/user?page='.($page+1));?>"><i class="icon-double-angle-right"></i></a></li>
                                    </ul>
                                </div>
                    </div>
                    <!--/.content-->
                </div>
                
                <script type="text/javascript">
				$(document).ready(function () {
					$("#filter").click(function(){
						event.preventDefault();
						value=$("#filter_text").val();
						window.location.replace('<?php echo URL::to('/user');?>'+'?name='+value);
					});
				});
				</script>
@stop