@extends('ib')

@section('content')
<?php
	$currentStatus = '';
	foreach ($logs as $row)
	{
		$currentStatus = $row->order_status;
	}
?>
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Order Detail</h3>
							</div>
							<div class="module-body">
                                <dl class="dl-horizontal">
                                  <dt>Order Number</dt>
                                  <dd><?php echo $order->order_number;?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                  <dt>Customer Name</dt>
                                  <dd><?php echo $order->customer_name;?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                  <dt>Customer Address</dt>
                                  <dd><?php echo $order->customer_address;?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                  <dt>Customer Phone</dt>
                                  <dd><?php echo $order->phone;?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                  <dt>Order Date</dt>
                                  <dd><?php echo $order->created_at;?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                  <dt>Current Status </dt>
                                  <dd><b class="<?php echo $currentStatus;?>-status"><?php echo $currentStatus;?></b></dd>
                               	</dl>
                                <?php if ($currentStatus=='canceled'){?>
                                <dl class="dl-horizontal">
                                  <dt>Cancel Reason</dt>
                                  <dd><?php echo $order->cancel_reason;?></dd>
                                </dl>
                                <?php }?>
                                <dl class="dl-horizontal">
                                  <dt>Update Status </dt>
                                  <dd>
                  
                                    <?php if ($privilege['access']==2){?>
                                  	<?php if ($order->status_id==1){?><a id="in-progress" class="btn btn-info">In Progress</a><?php }?>&nbsp;
                                	<?php if (($order->status_id==1)||($order->status_id==5)){?><a id="deliver" class="btn btn-primary">Deliver</a><?php }?>&nbsp;
                                 
                               		 <?php if ($order->status_id==1){?><a class="btn btn-danger cancel">Cancel</a><?php }?>&nbsp;
                                     <?php }?>
                                  </dd>
                                </dl>
                                
                                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>
                                                Item Name
                                            </th>
                                            <th>
                                            	Picture
                                            </th>
                                            <th>
                                                Comment
                                            </th>
                                            <th>
                                                Price
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
										$counter=1; 
										$subtotal=0;
										$shippingFee = 0;
										if ($order->shipping_fee!='')
											$shippingFee = $order->shipping_fee;
										foreach ($items as $row)
										{ 
											$subtotal+=$row->price;
									?>
                                    <tr>
                                    	<td><?php echo $counter;?></td>
                                    	<td><?php echo $row->product_name;?></td>
                                    	<td><img src="<?php echo $imageURL."product_image/thumb/".$row->product_img_1;?>"  style="height:100px;"></td>
                                    	<td><?php echo $row->comment;?></td>
                                    	<td>Rp <?php echo number_format($row->price);?></td>
                                    </tr>
                                    <?php $counter++;}?>
                                    <tr>
                                    	<td colspan="4" align="right"><strong>Sub Total</strong></td>
                                    	<td>Rp <?php echo number_format($subtotal);?></td>
                                    </tr>
                                    <tr>
                                    	<td colspan="4" align="right"><strong>Shipping Fee</strong></td>
                                    	<td>Rp <?php echo number_format($shippingFee);?></td>
                                    </tr>
                                    <tr>
                                    	<td colspan="4" align="right"><strong>Grand Total</strong></td>
                                    	<td>Rp <?php echo number_format($subtotal+$shippingFee);?></td>
                                    </tr>
                                    </tbody>
                              	</table>
                                <br>
                                <h2>Order History</h2>
                                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                <thead>
                                <tr>
                               		<th>No</th>
                               		<th>Date</th>
                               		<th>Status</th>
                               	</tr>
                                </thead>
                                <tbody>
                                <?php
									$counter = 1;
									/*
									foreach ($logs as $row)
									{
										?>
                                        <tr>
                                        	<td><?php echo $counter;?></td>
                                        	<td><?php echo $row->updated_at;?></td>
                                        	<td><b class="<?php echo $row->order_status;?>-status"><?php echo $row->order_status;?></b></td>
                                        </tr>
                                        <?
									}*/
									
									foreach ($logs as $row)
									{
										echo "
										<tr>
                                        	<td>".$counter."</td>
                                        	<td>".$row->updated_at."</td>
                                        	<td><b class=\"".$row->order_status."-status\">".$row->order_status."</b></td>
                                        </tr>";
									}
								?>
                                </tbody>
                                </table>
                                <br>
                            </div>
                      	</div>
                  	</div>
</div>

<script>
	$(document).ready(function () {
		$("#in-progress").click(function(event){
			$('#status').val(5);
			$('#update-form').submit();
		});
		$("#deliver").click(function(event){
			$('#status').val(3);
			$('#update-form').submit();
		});
		$(".cancel").click(function(event){
						//event.preventDefault();
						$('#cancel-form').modal({
							closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
							position: ["20%",],
							overlayId: 'confirm-overlay',
							containerId: 'confirm-container', 
							onShow: function (dialog) {
								var modal = this;
								/*
								$('.message', dialog.data[0]).append(message);
					
								// if the user clicks "yes"
								$('.yes', dialog.data[0]).click(function () {
									// call the callback
									if ($.isFunction(callback)) {
										callback.apply();
									}
									// close the dialog
									modal.close(); // or $.modal.close();
								});
								*/
							}
						});
						
					});
	});
</script>

<div id='cancel-form' style="display:none">
    <div class='header-modal'><span>Order Cancel </span></div>
    <form action="<?php echo URL::to('/order/cancel/') ?>" method="post" id="product_form">
    <input type="hidden" name="order_id" id="order_id" value="<?php echo $order->id;?>"/>
    <div class='message'>Reason &nbsp;:&nbsp; <textarea name="reason" required></textarea></div>
    <input type="submit" name="submit" value="submit" style="  margin-left: 10px;" />
    </form>
</div>
<form id="update-form" action="<?php echo URL::to('/order/update_status/') ?>" method="post">
<input type="hidden" name="order_id" value="<?php echo $order->id;?>">
<input type="hidden" name="status" id="status" value="">
</form>
@stop