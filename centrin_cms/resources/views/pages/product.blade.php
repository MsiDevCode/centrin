@extends('ib')

@section('content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Product</h3>
							</div>
							<div class="module-body">
								
                                <div class="module-option clearfix">
                                <form>
                                <div class="input-append pull-left">
                                    <input type="text" class="span3" placeholder="Filter by name..." id="filter_text">
                                    <button type="submit" class="btn" id="filter">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                                </form>
                                
                            </div>

								<div class="stream-list">
									<?php
										foreach ($products as $row)
										{
											?>
                                            <div class="media stream">
										<a href="#" class="media-avatar medium pull-left">
                                        <?php if ($row->profile_pic!=''){ ?>
											<img src="<?php echo $imageURL."profile_image/".$row->profile_pic;?>">
                                       	<?php }else{ ?>
											<img src="{{ URL::asset('images/user.png') }}">
                                      	<?php } ?>
                                            
										</a>
										<div class="media-body">
											<div class="stream-headline">
												<h5 class="stream-author">
													<?php echo $row->first_name;?> <?php echo $row->last_name;?> 
													<small><?php echo date('F d,Y', strtotime($row->inserted_on));?></small>
												</h5>
												<div class="stream-text">
													 <?php echo $row->product_name;?> 
												
                                              
                                                
                                                
                                               

                                                
                                                </div>
												<div class="stream-attachment photo">
													<div class="responsive-photo">
														<img src="<?php echo $imageURL."product_image/".$row->product_img_1;?>" />
													</div>
												</div>
                                                <?php
												if (trim($row->product_img_2)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_2;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_3)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_3;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}
												if (trim($row->product_img_4)!='')
												{
													?>
                                                    <div class="stream-attachment photo">
                                                        <div class="responsive-photo">
                                                            <img src="<?php echo $imageURL."product_image/".$row->product_img_4;?>" />
                                                        </div>
                                                    </div>
                                                    <?php
												}?>
                                                
											</div><!--/.stream-headline-->
                                                <div class="stream-options">
                                                    <a href="<?php echo URL::to('/product/edit/'.$row->id) ?>" class="btn btn-small">
                                                        <i class="icon-edit shaded"></i>
                                                        Edit
                                                    </a>
                                                    <a href="#" class="btn btn-small repost" product_id="<?php echo $row->id;?>">
                                                        <i class="icon-retweet shaded"></i>
                                                        Repost
                                                    </a>
                                                </div>
                                                <?php
												if (count($row->comments)>0)
												{
													?>
                                                    <div class="stream-respond">
                                                    <?php
													foreach ($row->comments as $rowC)
													{?>
                                                        <div class="media stream">
                                                            <a href="#" class="media-avatar small pull-left">
                                                                <img src="<?php echo $imageURL."profile_image/".$rowC->profile_pic;?>">
                                                            </a>
                                                            <div class="media-body">
                                                                <div class="stream-headline">
                                                                    <h5 class="stream-author">
                                                                        <?php echo $rowC->first_name;?> <?php echo $rowC->last_name;?> 
                                                                        <small><?php echo date('F d,Y', strtotime($rowC->commented_on));?></small>
                                                                    </h5>
                                                                    <div class="stream-text">
                                                                        <?php echo $rowC->comment_text;?>
                                                                    </div>
                                                                </div><!--/.stream-headline-->
                                                            </div>
                                                        </div><!--/.media .stream-->
                                                   	<?php } ?>
												
											</div>
                                                    <?php
												}
												?>
                                                

											
										</div>
									</div>
                                            <?php
										}
									?>
									

									<div class="pagination pagination-centered">
                                    <ul>
                                        <li><a href="<?php echo ($page>1)?URL::to('/product?page='.($page-1)):'#';?>"><i class="icon-double-angle-left"></i></a></li>
                                        <li><a href="<?php echo URL::to('/product?page='.($page+1));?>"><i class="icon-double-angle-right"></i></a></li>
                                    </ul>
                                </div>
								</div><!--/.stream-list-->
							</div><!--/.module-body-->
						</div><!--/.module-->
						
					</div><!--/.content-->
				</div>
                
<div id='repost-form' style="display:none">
                <div class='header-modal'><span>Product Repost </span></div>
                <form action="<?php echo URL::to('/product/repost/') ?>" method="post" id="product_form">
                <input type="hidden" name="product_id" id="product_id" value=""/>
                <div class='message'>Quantity &nbsp;:&nbsp; <input type="text" name="total" value="" style="height:30px;" required></div>
                <input type="submit" name="submit" value="submit" style="  margin-left: 10px;" />
                </form>
            </div>
                <script type="text/javascript">
				var limit=<?php echo $limit;?>;
				$(document).ready(function () {
					$("#show_more").click(function(){
						//event.preventDefault();
						alert('here');
					});
					
					$("#filter").click(function(){
						event.preventDefault();
						value=$("#filter_text").val();
						window.location.replace('<?php echo URL::to('/product');?>'+'?name='+value);
					});
					
				});
				$(document).ready(function () {
					
					$(".repost").click(function(event){
						var product_id = $(this).attr('product_id');
						$('#product_id').val(product_id);
						//event.preventDefault();
						$('#repost-form').modal({
							closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
							position: ["20%",],
							overlayId: 'confirm-overlay',
							containerId: 'confirm-container', 
							onShow: function (dialog) {
								var modal = this;
								/*
								$('.message', dialog.data[0]).append(message);
					
								// if the user clicks "yes"
								$('.yes', dialog.data[0]).click(function () {
									// call the callback
									if ($.isFunction(callback)) {
										callback.apply();
									}
									// close the dialog
									modal.close(); // or $.modal.close();
								});
								*/
							}
						});
						
					});
				});
				</script>
                
@stop
