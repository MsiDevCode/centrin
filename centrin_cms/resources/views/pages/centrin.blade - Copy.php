@extends('ib')

@section('content')

<div class="span9">
 	<div class="content">
                            <!--/#btn-controls-->
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Product Summary</h3>
                                </div>
                                
                                <div class="module-body">
                                <table>
                                	<tr>
                                    	<td><div id="stockchart" style="width: 400px; height: 500px;"></div></td>
                                    	<td><div id="statuschart" style="width: 400px; height: 500px;"></div></td>
                                    </tr>
                                </table>
                                <div id="categorychart" style="width: 100%; height: 600px;"></div>
                                
                                
                                </div>
                      		</div>
                            
	</div>
</div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
	  google.load('visualization', '1', {packages: ['corechart', 'bar']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Stock', 'Total'],
		  <?php
		  	foreach ($stockReports as $row)
			{
				echo "['".$row->stock_type."',".$row->total."],";
			}
		  ?>
        ]);

        var options = {
          title: 'Product Stock',
          pieHole: 0.4,
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('stockchart'));
        chart.draw(data, options);
		
		var productStatus =google.visualization.arrayToDataTable([
          ['Status', 'Total'],
		  <?php
		  	foreach ($statusReports as $row)
			{
				//echo "['".$row->status."',".$row->total."],";
			}
		  ?>
		   ['Inactive',2],['Active',2716],
        ]);
		
        var options2 = {
          title: 'Product Status',
          pieHole: 0.4,
        };
        var statuschart = new google.visualization.PieChart(document.getElementById('statuschart'));
        statuschart.draw(productStatus, options2);
		
		var categoryData = google.visualization.arrayToDataTable([
        	['Category', 'Total Product'],
			<?php
				foreach ($categoryReports as $row)
				{
					echo "[\"".$row->cat_name."\",".$row->total."],";
				}
			?>
      	]);
		
		  var categoryOptions = {
			title: 'Product Category',
			chartArea: {width: '50%'},
			hAxis: {
			  title: 'Total Product',
			  minValue: 0
			},
			vAxis: {
			  title: 'Category'
			}
		  };
	
		  var categoryChart = new google.visualization.BarChart(document.getElementById('categorychart'));
	
		  categoryChart.draw(categoryData, categoryOptions);
      }
    </script>
@stop