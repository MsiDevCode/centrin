@extends('ib')

@section('content')
<div class="span9">
    <div class="content">

        <div class="module">
            <div class="module-head">
                <h3>Group</h3>
            </div>
            <div class="module-body">
				<div class="module-body table">
                <?php if ($privilege['access']==2){?>
                <p><a class="btn btn-small btn-primary" href="<?php echo URL::to('/group/create/') ?>">Add New</a></p>
                <?php } ?>
                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                <thead>
                	<tr>
                    	<th>No</th>
                    	<th>Group Name</th>
                        <?php if ($privilege['access']==2){?>
                    	<th></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
					$counter = 1;
					foreach ($groups as $row)
					{?>
                    <tr>
                    	<td><?php echo $counter;?></td>
                    	<td><?php echo $row->name;?></td>
                        
                        <?php if ($privilege['access']==2){?>
                    	<td><a class="btn-box small" href="<?php echo URL::to('/group/edit/'.$row->id) ?>"><i class="icon-edit"></i></a></td>
                        <?php } ?>
                    </tr>
					<?php 
						$counter++;
					}
				?>
                </tbody>
                </table>
                </div>
            </div>
     	</div>
	</div>
</div>
@stop