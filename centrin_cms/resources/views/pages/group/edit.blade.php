@extends('ib')

@section('content')

				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Create Product</h3>
							</div>
							<div class="module-body">


									<form class="form-horizontal row-fluid" id="product_form" enctype="multipart/form-data" action="<?php echo URL::to('/group/update');?>" method="post">
                                    	<input type="hidden" name="prev" value="<?php echo $_SERVER['HTTP_REFERER'];?>" />
                                    	<input type="hidden" name="id" value="<?php echo $group->id;?>">
										<div class="control-group">
											<label class="control-label" for="basicinput">Group Name</label>
											<div class="controls">
												<input type="text" id="name" name="name" placeholder="Group Name" value="<?php echo $group->name;?>" class="span8" required>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" name="description" id="description" rows="5" placeholder="Description"><?php echo $group->description;?></textarea>
											</div>
										</div>
									  <p></p>
										
										<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                        <thead>
                                        	<tr>
                                            	<th>&nbsp;</th>
                                            	<th colspan="3" style="text-align: center;">Access</th>
                                            	<th colspan="2" style="text-align: center;">Data</th>
                                           	</tr>
                                        	<tr>
                                            	<th>Privilege</th>
                                            	<th style="text-align: center;">Write</th>
                                            	<th style="text-align: center;">Read</th>
                                            	<th style="text-align: center;">Non</th>
                                            	<th style="text-align: center;">Own Data</th>
                                            	<th style="text-align: center;">All Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$counter = 1;
											foreach ($privileges as $row)
											{
												if ($row->parent==0){
										?>
                                        	<tr>
                                            	<td><strong><?php echo $row->name;?></strong></td>
                                                <?php if ($row->url!='#'){?>
                                            	<td><input type="radio" name="access_type_<?php echo $row->id;?>" value="2"  <?php if ($groupPrivileges[$row->id]['access_type']==2) echo 'checked';?>></td>
                                            	<td><input type="radio" name="access_type_<?php echo $row->id;?>" value="1" <?php if ($groupPrivileges[$row->id]['access_type']==1) echo 'checked';?>></td>
                                            	<td><input type="radio" name="access_type_<?php echo $row->id;?>" value="0" <?php if ($groupPrivileges[$row->id]['access_type']==0) echo 'checked';?>></td>
                                            	<td><input type="radio" name="data_type_<?php echo $row->id;?>" value="0" <?php if ($groupPrivileges[$row->id]['data_type']==0) echo 'checked';?>></td>
                                                <td><input type="radio" name="data_type_<?php echo $row->id;?>" value="1" <?php if ($groupPrivileges[$row->id]['data_type']==1) echo 'checked';?>></td>
                                                <?php } ?>

                                            </tr>
                                        <?php
													foreach ($privileges as $rowC)
													{
														if ($rowC->parent==$row->id)
														{
															?>
                                                            <tr>
                                                                <td>&nbsp;&nbsp;&nbsp;<?php echo $rowC->name;?></td>
                                                                <td><input type="radio" name="access_type_<?php echo $rowC->id;?>" value="2" <?php if ($groupPrivileges[$rowC->id]['access_type']==2) echo 'checked';?>></td>
                                                                <td><input type="radio" name="access_type_<?php echo $rowC->id;?>" value="1" <?php if ($groupPrivileges[$rowC->id]['access_type']==1) echo 'checked';?>></td>
                                                                <td><input type="radio" name="access_type_<?php echo $rowC->id;?>" value="0" <?php if ($groupPrivileges[$rowC->id]['access_type']==0) echo 'checked';?>></td>
                                                                <td><input type="radio" name="data_type_<?php echo $rowC->id;?>" value="0" <?php if ($groupPrivileges[$rowC->id]['data_type']==0) echo 'checked';?>></td>
                                                                <td><input type="radio" name="data_type_<?php echo $rowC->id;?>" value="1" <?php if ($groupPrivileges[$rowC->id]['data_type']==1) echo 'checked';?>></td>
                
                                                            </tr>
                                                            <?php
														}
													}
												}
											}
										?>
                                        </tbody>
                                        </table>
										
                                        <p></p>
                                        <p></p>
                                        <p></p>
	
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

						
						
					</div><!--/.content-->
				</div>
@stop