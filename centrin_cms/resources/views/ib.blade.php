<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Instabeli</title>
        <link type="text/css" href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/theme.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/confirm.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('images/icons/css/font-awesome.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/timepicki.css') }}" rel="stylesheet">
      	
        <script src="{{ URL::asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/flot/jquery.flot.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/jquery.simplemodal.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/ib.common.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('scripts/timepicki.js') }}" type="text/javascript"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Instabeli </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                    	<!--
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>
                        -->
                        <ul class="nav pull-right">
                        	<!--
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Item No. 1</a></li>
                                    <li><a href="#">Don't Click</a></li>
                                    <li class="divider"></li>
                                    <li class="nav-header">Example Header</li>
                                    <li><a href="#">A Separated link</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Support </a></li>
                            -->
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            	<?php if (Session::get('profile_pic')!=''){?>
                                <img src="<?php echo Session::get('profile_pic');?>" class="nav-avatar" />
                                <?php } else {?>
                                <img src="{{ URL::asset('images/user.png') }}" class="nav-avatar" />
                                <?php } ?>
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo URL::to('/profile/edit') ?>">Edit Profile</a></li>
                                    <li><a href="<?php echo URL::to('/profile/change_password') ?>">Change Password</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo URL::to('/logout') ?>">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    @include('menu')
                    <!--/.span3-->
                    @yield('content')
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
        		@include('footer')
        </div>
          <div id='confirm'>
                <div class='header-modal'><span>Confirm</span></div>
                <div class='message'></div>
                <div class='buttons'>
                    <div class='no simplemodal-close'>No</div><div class='yes'>Yes</div>
                </div>
            </div>
            
    </body>
