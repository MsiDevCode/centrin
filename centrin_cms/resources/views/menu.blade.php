<?php
	//print_r(Session::get('privileges'));die();
?>
<div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                            	<?php 
								foreach (Session::get('privileges') as $row)
								{
									if (($row->url!='#')&&($row->access_type>0)&&($row->parent==0))
									{
										?>
                                        <li class="active"><a href="<?php echo URL::to('/'.$row->url) ?>"><i class="menu-icon icon-<?php echo $row->icon;?>"></i><?php echo $row->name;?></a></li> 
                                        <?php
									}else 
									{
										$totalAccess = 0;
										foreach (Session::get('privileges') as $rowC)
										{
											if (($rowC->parent==$row->id)&&($rowC->access_type>0))
											{
												$totalAccess++;
											}
										}
										if ($totalAccess>0)
										{
											?>
                                      	<li><a class="collapsed" data-toggle="collapse" href="#togglePages<?php echo $row->id;?>"><i class="menu-icon icon-<?php echo $row->icon;?>">
                                        </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                        
                                        </i><?php echo $row->name;?> </a>
                                            <ul id="togglePages<?php echo $row->id;?>" class="unstyled collapse" style="height: 0px;">
                                               
                                                <?php
												foreach (Session::get('privileges') as $rowC)
												{
													if (($rowC->parent==$row->id)&&($rowC->access_type>0))
													{ 
													?>
													<li><a href="<?php echo URL::to('/'.$rowC->url) ?>"><i class="menu-icon icon-<?php echo $rowC->icon;?>"></i><?php echo $rowC->name;?> </a>
													<?php
													}
                                                }
												?>
                                                
                                            </ul>
                                        </li>
                                            <?php
										}
									}
								}
								?>
                            </ul>
                       	</div>
</div>
<!--
<div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <?php if (Session::get('is_admin')==1){?>
                                <li class="active"><a href="<?php echo URL::to('/dashboard') ?>"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li> 
                                <li class="active"><a href="<?php echo URL::to('/group') ?>"><i class="menu-icon icon-group"></i>Group
                                </a></li> 
                                <?php } ?>
                                
                                <?php if (Session::get('is_admin')==1){?>
                                
                                 <li><a class="collapsed" data-toggle="collapse" href="#togglePages2"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                
                                </i>Category </a>
                                    <ul id="togglePages2" class="unstyled collapse" style="height: 0px;">
                                       
                                        <li><a href="<?php echo URL::to('/category') ?>"><i class="menu-icon icon-table"></i>All Categories </a>
                                        
                                         <?php if (Session::get('is_admin')==1){?>
                                        <li><a href="<?php echo URL::to('/category/create') ?>"><i class="menu-icon icon-table"></i>Add Category </a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php }?>
                                
                                <li><a href="<?php echo URL::to('/product/create') ?>"><i class="menu-icon icon-paste"></i>Upload Product </a></li>
                                <li><a href="<?php echo URL::to('/catalog') ?>"><i class="menu-icon icon-bullhorn"></i>Products </a>
                                <?php if (Session::get('is_admin')==1){?>
                                 <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Users </a>
                                    <ul id="togglePages" class="unstyled collapse" style="height: 0px;">
                                       
                                        <li><a href="<?php echo URL::to('/user') ?>"><i class="menu-icon icon-table"></i>All Users </a>
                                        <li><a href="<?php echo URL::to('/user/create') ?>"><i class="menu-icon icon-table"></i>Add New User </a>
                                        
                                    </ul>
                                </li>
                                <?php }?>
                                <li><a href="<?php echo URL::to('/order') ?>"><i class="menu-icon icon-bullhorn"></i>Orders </a>
                             
                                
                               
                                <!--
                                </li>
                                <li><a href="message.html"><i class="menu-icon icon-inbox"></i>Inbox <b class="label green pull-right">
                                    11</b> </a></li>
                                <li><a href="task.html"><i class="menu-icon icon-tasks"></i>Tasks <b class="label orange pull-right">
                                    19</b> </a></li>-->
                            </ul>
                            <!--/.widget-nav-->
                            
                            <!--
                            <ul class="widget widget-menu unstyled">
                                <li><a href="ui-button-icon.html"><i class="menu-icon icon-bold"></i> Buttons </a></li>
                                <li><a href="ui-typography.html"><i class="menu-icon icon-book"></i>Typography </a></li>
                                <li><a href="form.html"><i class="menu-icon icon-paste"></i>Forms </a></li>
                                <li><a href="table.html"><i class="menu-icon icon-table"></i>Tables </a></li>
                                <li><a href="charts.html"><i class="menu-icon icon-bar-chart"></i>Charts </a></li>
                            </ul>
                            <!--/.widget-nav-->
                            <!--
                            <ul class="widget widget-menu unstyled">
                                <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>More Pages </a>
                                    <ul id="togglePages" class="collapse unstyled">
                                        <li><a href="other-login.html"><i class="icon-inbox"></i>Login </a></li>
                                        <li><a href="other-user-profile.html"><i class="icon-inbox"></i>Profile </a></li>
                                        <li><a href="other-user-listing.html"><i class="icon-inbox"></i>All Users </a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="menu-icon icon-signout"></i>Logout </a></li>
                            </ul>
                            
                        </div>
                    </div>
                    -->
                    
                        <!--/.sidebar-->