<?php namespace App\Http\Middleware;

use Closure;
use Session;

class LoginMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//Session::put('key', 'value');
		$email =  Session::get('email');
		if ($email=='')
                {
                    return redirect('login');
                }
		return $next($request);
	}

}
