<?php namespace App\Http\Middleware;

use Closure;
use Session;

class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//Session::put('key', 'value');
		$email =  Session::get('email');
		$isAdmin =  Session::get('is_admin');
		if (($email=='')||($isAdmin==0))
                {
                    return redirect('login');
                }
		return $next($request);
	}

}
