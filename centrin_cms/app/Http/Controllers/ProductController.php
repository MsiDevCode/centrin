<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lib\Image;

use App\Product;
use App\Category;
use App\Transaction;
use App\User;
use Config;
use URL;
use Session;
use Input;

use Illuminate\Http\Request;

class ProductController extends Controller {

	
	
	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 7;
		$this->checkAccessType(1);
	}
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$name = Input::get('name');
		$page = (Input::get('page')=='')?1:Input::get('page');	
		$start = ($page-1)*$this->perPage;
		//echo $start;die();
		$data = array();
		$data['imageURL'] =  Config::get('app.image_url');
                
		$isAdmin =  Session::get('is_admin');
                $products = array();
                if ($isAdmin==1)
                    $products = Product::getLatestProduct(0,$name,$start,$this->perPage);
                else
                    $products = Product::getLatestProduct(Session::get('id'),$name,$start,$this->perPage);
		
                for ($i=0;$i<count($products);$i++)
		{
			$products[$i]->comments = User::getProductComment($products[$i]->id);
		}
		$data['products'] = $products;
		$data['limit'] = $this->perPage;
		$data['page'] = $page; 
		//print_r($products);die();
		return view('pages.product',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$status = Input::get('status');
		$data = array();
		$categories = Category::all();
		$data['categories'] = $categories;
		$data['status'] = $status;
		$data['privilege'] = $this->getPrivilege();
		return view('pages.product.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$uploadPath = Config::get('app.upload_path').'product_image/';
		$img1 =Input::file('img_1');
		$productImg1 = "product_photo_1_".Session::get('id')."_".time().".".$img1->guessClientExtension();
		$img1->move($uploadPath,$productImg1);
		$filenameThumb= Config::get('app.upload_path').'product_image/thumb/'.$productImg1;
		if (file_exists($uploadPath.$productImg1))
		{
			@Image::makeThumbnail($uploadPath.$productImg1,$filenameThumb,525);
			if (($img1->guessClientExtension()!='jpg')||($img1->guessClientExtension()!='jpeg'))
				@copy($uploadPath.$productImg1,$filenameThumb);
		}
		
		$img2 =Input::file('img_2');
		$productImg2 = '';
		
		if (@$img2!='')
		{
			$productImg2 = "product_photo_2_".Session::get('id')."_".time().".".$img2->guessClientExtension();
			$img2->move($uploadPath,$productImg2);
		}
		
		$img3 =Input::file('img_3');
		$productImg3 = '';
		
		
		if (@$img3!='')
		{
			$productImg3 = "product_photo_3_".Session::get('id')."_".time().".".$img3->guessClientExtension();
			$img3->move($uploadPath,$productImg3);
		}
		
		$img4 =Input::file('img_4');
		$productImg4 = '';
		
		
		if (@$img4!='')
		{
			$productImg4 = "product_photo_4_".Session::get('id')."_".time().".".$img4->guessClientExtension();
			$img4->move($uploadPath,$productImg4);
		}
		
		$input = Input::all();
		
		$product = new Product;
		$product->cat_id = $input['category'];
		$product->user_id = Session::get('id');
		$product->price = $input['price'];
		$product->stage = 1;
		$product->likes = 0;
		$product->city = 0;
		$product->state = 0;
		
		$isActive = @$input['is_active'];
		
		if ($isActive=='')
			$isActive = 0;
		
		$product->product_name = $input['title'];
		$product->description = $input['desc'];
		$product->product_img_1 = $productImg1;
		$product->product_img_2 = $productImg2;
		$product->product_img_3 = $productImg3;
		$product->product_img_4 = $productImg4;
		$product->is_wanted = 1;
		$product->stock_type = $input['stock_type'];
		$product->stock_amount = 0;
		$product->location = '';
		$product->lat = 0;
		$product->long = 0;
		$product->inserted_on = time();
		$product->sold_on = 0;
		$product->updated_on = 0;
		$product->share_facebook = 0;
		$product->share_twitter = 0;
		$product->productHash = '';
		$product->isActive = $isActive;
		$product->timestamps = false;
		$product->save();
		$productId = $product->id;
		$product->createHash($input['desc'],$productId);
		return redirect('product/create?status=1');
		//echo  $img1->move('c:\\xampp\\htdocs\\',$productImg1);
		//print_r($img1);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		
		$product = Product::find($id);
		if (@$product->exists)
		{
			//print_r($product);
			$data = array();
			$categories = Category::all();
			$data['categories'] = $categories;
			$data['product'] = $product;
			$data['privilege'] = $this->getPrivilege();
			$data['imageURL'] =  Config::get('app.image_url');
			return view('pages.product.edit',$data);
		}
		else
			return redirect('product');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
		
		$uploadPath = Config::get('app.upload_path').'product_image/';
		$img1 =Input::file('img_1');
		$productImg1 = '';
		
		if (@$img1!='')
		{
			$productImg1 = "product_photo_1_".Session::get('id')."_".time().".".$img1->guessClientExtension();
			$img1->move($uploadPath,$productImg1);
			$filenameThumb= Config::get('app.upload_path').'product_image/thumb/'.$productImg1;
			if (file_exists($uploadPath.$productImg1))
			{
				@Image::makeThumbnail($uploadPath.$productImg1,$filenameThumb,525);
				if (($img1->guessClientExtension()!='jpg')||($img1->guessClientExtension()!='jpeg'))
					@copy($uploadPath.$productImg1,$filenameThumb);
			}
		}
		
		$img2 =Input::file('img_2');
		$productImg2 = '';
		
		if (@$img2!='')
		{
			$productImg2 = "product_photo_2_".Session::get('id')."_".time().".".$img2->guessClientExtension();
			$img2->move($uploadPath,$productImg2);
		}
		
		$img3 =Input::file('img_3');
		$productImg3 = '';
		
		
		if (@$img3!='')
		{
			$productImg3 = "product_photo_3_".Session::get('id')."_".time().".".$img3->guessClientExtension();
			$img3->move($uploadPath,$productImg3);
		}
		
		$img4 =Input::file('img_4');
		$productImg4 = '';
		
		
		if (@$img4!='')
		{
			$productImg4 = "product_photo_4_".Session::get('id')."_".time().".".$img4->guessClientExtension();
			$img4->move($uploadPath,$productImg4);
		}
		
		$input = Input::all();
		
		$isActive = @$input['is_active'];
		
		if ($isActive=='')
			$isActive = 0;
		
		$product = Product::find($input['id']);
		$product->cat_id = $input['category'];
		$product->price = $input['price'];
		$product->isActive = $isActive;
		
		$product->product_name = $input['title'];
		$product->description = $input['desc'];
		$product->stock_type = $input['stock_type'];
		if ($productImg1!='')
			$product->product_img_1 = $productImg1;
		if ($productImg2!='')
			$product->product_img_2 = $productImg2;
		if ($productImg3!='')
			$product->product_img_3 = $productImg3;
		if ($productImg4!='')
			$product->product_img_4 = $productImg4;
			
		$product->updated_on = time();
		$product->timestamps = false;
		$product->save();
		
		if (($isActive==0)||($product->stock_type==0))
		{
			//Product::removeFromCart($product->id);
		}
		
		$prev = $input['prev'];
		if ($prev!='')
			return redirect($prev);
		else
			return redirect('product');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function repost()
	{
		$input = Input::all();
		$id = $input['product_id'];
		$total = $input['total'];
		
		Product::repost($id,$total);
		return redirect('product');
	}

}
