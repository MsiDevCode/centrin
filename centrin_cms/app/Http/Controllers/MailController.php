<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Category;
use App\SalesOrder;
use App\SalesOrderItem;
use App\SalesOrderHistory;
use App\SalesOrderStatus;
use App\Transaction;
use App\User;
use Config;
use URL;
use Session;
use Input;
use App;
use Mail;


use Illuminate\Http\Request;
class MailController extends Controller {
	
	public function sendEmailNotification($id,$status)
	{
		$order = SalesOrder::find($id);
		if (@$order->exists)
		{
			$input = array();
			$input['order'] = $order;
			Mail::send('emails.orderstatus', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				//$message->to('listian.pratomo@gmail.com');
				$message->to('christiankhoe91@gmail.com');
				$message->subject('Your order('.$input['order']->order_number.') has been updated!');
			});
		}
	}
	
	
	public function sendEmailNotificationCancel($id,$status)
	{
		$order = SalesOrder::find($id);
		if (@$order->exists)
		{
			$input = array();
			$input['order'] = $order;
			Mail::send('emails.orderstatus', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				//$message->to('listian.pratomo@gmail.com');
				$message->to('christiankhoe91@gmail.com');
				$message->subject('Your order ('.$input['order']->order_number.') has been canceled!');
			});
		}
	}
	
	public function registerMail($id,$password)
	{
		$user = User::find($id);
		if (@$user->exists)
		{
			$input = array();
			$input['email'] = $user->email;
			$input['first_name'] = $user->first_name;
			$input['last_name'] = $user->last_name;
			$input['password'] = $password;
			$input['url'] = 'http://apartement.centrin.net.id/seller/public';
			Mail::send('emails.register', $input, function($message) use ($input)
			{
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to($input['email']);
				$message->subject('You instabeli account has been created!');
			});
		}
	}
	
	public function resetMail($id,$password)
	{
		$user = User::find($id);
		if (@$user->exists)
		{
			$input = array();
			$input['email'] = $user->email;
			$input['first_name'] = $user->first_name;
			$input['last_name'] = $user->last_name;
			$input['password'] = $password;
			$input['url'] = 'http://apartement.centrin.net.id/seller/public';
			Mail::send('emails.reset', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to($input['email']);
				$message->subject('Your instabeli password has been reset!');
			});
		}
	}
}