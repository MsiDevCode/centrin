<?php namespace App\Http\Controllers;

use App\Lib\Image;
use App\Http\Requests;
use App\Product;
use App\Http\Controllers\Controller;
use Config;
use Mail;


use Illuminate\Http\Request;

class TestController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		ini_set('max_execution_time',100000);
		//Image::makeThumbnail('http://localhost/centrin_cms/public/uploads/product_image/product_photo_1_592_1427702853.jpg','C:/xampp/htdocs/centrin_cms/public/uploads/product_image/test.jpeg',300);
		
		$all = Product::all();
		//print_r($all);
		foreach ($all as $row)
		{
			$filename= Config::get('app.upload_path').'product_image/'.$row->product_img_1;
			$filenameThumb= Config::get('app.upload_path').'product_image/thumb/'.$row->product_img_1;
			if (file_exists($filename))
			{
				@Image::makeThumbnail($filename,$filenameThumb,300);
			}else
			{
				echo $filename."<br>";
			}
		}
	}
	
	public function sendEmail()
	{
		ini_set('max_execution_time',1000);
		Mail::send('mail', array(), function($message)
		{
			$message->from('service.instabeli@gmail.com', 'Instabeli');
			$message->to('listian.pratomo@gmail.com');
			$message->subject('Test Email!');
		});
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
