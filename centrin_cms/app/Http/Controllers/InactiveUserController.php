<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bank;
use App\BankDetail;
use App\Product;
use App\Transaction;
use App\User;
use Config;
use URL;
use Input;
use Session;

use Illuminate\Http\Request;

class InactiveUserController extends Controller {


	public function __construct()
	{
		$this->middleware('login');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$page = (Input::get('page')=='')?1:Input::get('page');
		
		$start = ($page-1)*$this->perPage;
		
		//die($name);
		$data['imageURL'] =  Config::get('app.image_url');
		$data['page'] = $page; 
		$data['users'] = User::getInsactiveUser($start,$this->perPage);
		return view('pages.user.inactive',$data);
	}
	
	public function submit()
	{
		//print_r($_POST);
		$status = Input::get('status');
		$users = Input::get('users');
		if (is_array($users))
		{
			User::updateStatusUser($status,$users);
		}
		return redirect($_SERVER['HTTP_REFERER']);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
