<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Config;
use App\User;
use App\IbUserGroup;
use Mail;
use URL;

use Illuminate\Http\Request;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $email =  Session::get('email');
		$isAdmin =  Session::get('is_admin');
                if ($email!='')
                {
                    if ($isAdmin==1)
                        return redirect('dashboard');
                    else
                        return redirect('product');
                        
                }
		return view('pages.user.login');
	}
	
	public function auth()
	{
		$input = Input::all();
		$email = $input['email'];
		$password = $input['password'];
		$detail = User::getUserByEmail($email);
		if (count($detail)>0)
		{
			if (@$detail[0]->passwd==md5($password))
			{
				Session::put('id', $detail[0]->id);
				Session::put('email', $email);
				Session::put('password', $password);
				Session::put('is_admin', $detail[0]->is_admin);
				$profile_pic='';
				if (trim($detail[0]->profile_pic)!='')
					$profile_pic=Config::get('app.image_url')."profile_image/".$detail[0]->profile_pic;
				Session::put('profile_pic', $profile_pic);
				$groups = IbUserGroup::getUserGroup($detail[0]->id);
				@Session::put('group_id', $groups[0]->group_id);
				Session::put('privileges',IbUserGroup::getUserPrivilege($detail[0]->id));
                                if ($detail[0]->is_admin==1)
                                    return redirect('dashboard');
                                else
                                    return redirect('product');
			}else{
				
				Session::flash('error', 'Email & password don\'t match.');
				return redirect('login');
			}
		}else
		{
			Session::flash('error', 'Your email is not registered.');
			return redirect('login');
		}
	}
	
	public function forgetPassword()
	{
		return view('pages.user.forgetpassword');
	}
	
	public function resetPassword()
	{
		$input = Input::all();
		$email = $input['email'];
		$detail = User::getUserByEmail($email);
		if (count($detail)>0)
		{
			$user = User::find( $detail[0]->id);
		
			if (@$user->exists)
			{
				$user->timestamps = false;
				$password = uniqid();
				$user->passwd = md5($password);
				$user->save();
				$input =array();
				
				$input['password'] = $password;
				$input['email'] = $user->email;
				$input['url'] = Config::get('app.url');
				/*
				Mail::send('emails.reset', $input, function($message) use ($input)
				{
					//print_r($message);die();
					$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
					$message->to($input['email']);
					$message->subject('Your instabeli password has been reset!');
				});*/
				@file_get_contents("http://api.instabeli.com/centrin_cms/public/mail_reset/".$user->id."/$password");
			}
			
			Session::flash('success', 'Your password has been sent to your email account.');
			return redirect('forget_password');
		}else
		{
			Session::flash('error', 'Your email is not registered.');
			return redirect('forget_password');
		}
	}
	
	public function out()
	{
		Session::flush();
		return redirect('login');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
