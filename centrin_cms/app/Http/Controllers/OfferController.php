<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Activity;
use App\Product;
use App\Category;
use App\Offer;
use App\Transaction;
use App\User;
use App\AndroidPush;
use Config;
use URL;
use Session;
use Input;

use Illuminate\Http\Request;

class OfferController extends Controller {


	public function __construct()
	{
		$this->middleware('login');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$page = (Input::get('page')=='')?1:Input::get('page');	
		$start = ($page-1)*$this->perPage;
                
		$data = array();
		$data['imageURL'] =  Config::get('app.image_url');
		$isAdmin =  Session::get('is_admin');
                $offers = array();
                if ($isAdmin==1)
                    $offers =  Offer::getNewOffer(0,$start,$this->perPage);
                else
                    $offers =  Offer::getNewOffer(Session::get('id'),$start,$this->perPage);
		$data['offers'] = $offers;
		$data['limit'] = $this->perPage;
		$data['page'] = $page; 
		return view('pages.offer',$data);
	}

	public function accept($id)
	{
		$detail = Offer::getOfferDetail($id);
		if (count($detail)>0)
		{
			Offer::acceptOffer($id);
			$receiver = $detail[0]->id_o;
			//$receiver = '594';
			$message = $detail[0]->user_name." has accepted your offer for ".$detail[0]->product_name;
			AndroidPush::sendNotification($receiver,$message, 'sold notification', 'Offer Accepted!', $detail[0]->id);
			$activity = new Activity;
			$activity->timestamps = false;
			$activity->receiver_id = $detail[0]->id_o ;
			$activity->user_id = $detail[0]->user_id ;
			$activity->status = 1 ;
			$activity->product_id = $detail[0]->id ;
			$activity->offer_id = $detail[0]->offer_id ;
			$activity->activity_type = 8 ;
			$activity->message_text = $message ;
			$activity->activity_time = time() ;
			$activity->save();
			
			
			$activity = new Activity;
			$activity->timestamps = false;
			$activity->receiver_id = $detail[0]->user_id ;
			$activity->user_id = $detail[0]->id_o ;
			$activity->status = 1 ;
			$activity->product_id = $detail[0]->id ;
			$activity->offer_id = $detail[0]->offer_id ;
			$activity->activity_type = 8 ;
			$message = "You have accepted " .$detail[0]->user_name_o. "'s offer for " .$detail[0]->product_name;
			$activity->message_text = $message ;
			$activity->activity_time = time() ;
			$activity->save();
		}
		return redirect('offer');
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
