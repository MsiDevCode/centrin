<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Category;
use App\SalesOrder;
use App\SalesOrderItem;
use App\SalesOrderHistory;
use App\SalesOrderStatus;
use App\Transaction;
use App\User;
use Config;
use URL;
use Session;
use Input;
use App;
use Mail;


use Illuminate\Http\Request;
class OrderController extends Controller {

	
	
	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 11;
		$this->checkAccessType(1);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$orderNumber = Input::get('order_number');
		$orderStatus = Input::get('order_status');
		$from = Input::get('from');
		$to = Input::get('to');
		
		if ($orderStatus=='')
			$orderStatus=0;
		$page = (Input::get('page')=='')?1:Input::get('page');	
		$start = ($page-1)*$this->perPage;
		$products = array();$privileges = $this->getPrivilege();
		$isAdmin =  0;
		if ($privileges['data']==1)
			$isAdmin = 1;
		if ($isAdmin==1)
			$orders = SalesOrder::getOrders(0,$orderNumber,$orderStatus,$from,$to,$start,$this->perPage);
		else
			$orders = SalesOrder::getOrders(Session::get('id'),$orderNumber,$orderStatus,$from,$to,$start,$this->perPage);
		
		
		$status = SalesOrderStatus::all();
		
		$data =array();
		$data['orderNumber'] = $orderNumber;
		$data['orderStatus'] = $orderStatus;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['status'] = $status;
		$data['orders'] = $orders;
		$data['start'] = $start;
		$data['limit'] = $this->perPage;
		$data['page'] = $page; 
		$data['privilege'] = $this->getPrivilege();
		//print_r($products);die();
		return view('pages.order',$data);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$orderDetail = SalesOrder::find($id);
		//print_r($orderDetail);die();
		if (@$orderDetail->exists)
		{
			$items = SalesOrderItem::getOrderItem($id);
			$data = array();
			$data['order'] = $orderDetail;
			$data['items'] = $items;
			
			$data['logs'] = SalesOrderHistory::getHistory($id);
			$data['imageURL'] =  Config::get('app.image_url');
			$data['privilege'] = $this->getPrivilege();
			return view('pages.order.detail',$data);
		}else
		{
			return redirect('order');
		}
	}
	
	public function updateStatus()
	{
		$input = Input::all();
		$id = $input['order_id'];
		$status = $input['status'];
		
		$order = SalesOrder::find($id);
		if (@$order->exists)
		{
			$order->status_id = $status;
			//$order->cancel_reason = $reason;
			//$order->timestamps = false;
			$order->save();
			SalesOrder::createLog($id);
			$input = array();
			$input['order'] = $order;
			/*
			Mail::send('emails.orderstatus', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to('christiankhoe91@gmail.com');
				$message->subject('Your order('.$input['order']->order_number.') has been updated!');
			});
			*/
			
			@file_get_contents("http://api.instabeli.com/centrin_cms/public/mail/$id/$status");
		}
		return redirect($_SERVER['HTTP_REFERER']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function printInvoice($id)
	{
		$pdf = App::make('dompdf');
		$data = array();
		$data['order'] = SalesOrder::find($id);
		$data['orderItems'] = SalesOrderItem::getOrderItem($id);
		//return view('print.invoice',$data);
        $pdf->loadHTML(view('print.invoice',$data));
       	return $pdf->download();
	}
	
	public function cancel()
	{
		$input = Input::all();
		$id = $input['order_id'];
		$reason = $input['reason'];
		
		$order = SalesOrder::find($id);
		if (@$order->exists)
		{
			$order->status_id = 2;
			$order->cancel_reason = $reason;
			//$order->timestamps = false;
			$order->save();
			SalesOrder::createLog($id);
			$input = array();
			$input['order'] = $order;
			/*
			Mail::send('emails.orderstatus', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to('christiankhoe91@gmail.com');
				$message->subject('Your order ('.$input['order']->order_number.') has been canceled!');
			});*/
			@file_get_contents("http://api.instabeli.com/centrin_cms/public/mail_cancel/$id/2");
		}
		return redirect($_SERVER['HTTP_REFERER']);
	}

}
