<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IbGroup;
use App\IbPrivilege;
use App\IbGroupPrivilege;
use Config;
use URL;
use Session;
use Input;

class GroupController extends Controller {

	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 2;
		$this->checkAccessType(1);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['privilege'] = $this->getPrivilege();
		$data['groups'] = IbGroup::all();
		return view('pages.group',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array();
		$data['privileges'] = IbPrivilege::all();
		
		return view('pages.group.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$privileges = IbPrivilege::all();
		$group = new IbGroup;
		$group->name = $input['name'];
		$group->description = $input['description'];
		$group->save();
		
		$data = array();
		foreach ($privileges as $row)
		{
			$tmp =  (object) array();
			$tmp->privilege_id = $row->id;
			$dataType = @($input['data_type_'.$row->id]=='')?0:$input['data_type_'.$row->id];
			$accessType = @($input['access_type_'.$row->id]=='')?0:$input['access_type_'.$row->id];
			$tmp->data_type = $dataType;
			$tmp->access_type = $accessType;
			array_push($data,$tmp);
		}
		IbGroupPrivilege::createGroupPrivilege($group->id,$data);
		return redirect('group');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$group = IbGroup::find($id);
		if (@$group->exists)
		{
			//print_r($product);
			$data = array();
			$data['group'] = $group;
			$data['privileges'] = IbPrivilege::all();
			$privData = IbGroupPrivilege::getPrivilegeByGroup($id);
			$groupPrivileges = array();
			foreach ($privData as $row)
			{
				//print_r($row);die();
				$pid = $row->id;
				$groupPrivileges[$pid]['access_type'] = ($row->access_type=='')?0:$row->access_type;
				$groupPrivileges[$pid]['data_type'] = ($row->data_type=='')?0:$row->data_type;
			}
			$data['groupPrivileges'] = $groupPrivileges;
			return view('pages.group.edit',$data);
		}
		else
			return redirect('group');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();
		$group = IbGroup::find($input['id']);
		if (@$group->exists)
		{
			$group->name = $input['name'];
			$group->description = $input['description'];
			$group->save();
			$privileges = IbPrivilege::all();
			$data = array();
			foreach ($privileges as $row)
			{
				$tmp =  (object) array();
				$tmp->privilege_id = $row->id;
				$dataType = @($input['data_type_'.$row->id]=='')?0:$input['data_type_'.$row->id];
				$accessType = @($input['access_type_'.$row->id]=='')?0:$input['access_type_'.$row->id];
				$tmp->data_type = $dataType;
				$tmp->access_type = $accessType;
				array_push($data,$tmp);
			}
			IbGroupPrivilege::createGroupPrivilege($group->id,$data);
		}
		return redirect('group');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
