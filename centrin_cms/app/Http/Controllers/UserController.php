<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bank;
use App\BankDetail;
use App\Product;
use App\Transaction;
use App\User;
use App\IbGroup;
use App\IbUserGroup;
use App\IbGroupPrivilege;
use Config;
use URL;
use Input;
use Session;
use Mail;


use Illuminate\Http\Request;

class UserController extends Controller {


	public function __construct()
	{
        $this->middleware('login');
		$this->privilegeId = 8;
		$this->checkAccessType(1);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$name = Input::get('name');
		$page = (Input::get('page')=='')?1:Input::get('page');
		
		$start = ($page-1)*$this->perPage;
		
		//die($name);
		$data['imageURL'] =  Config::get('app.image_url');
		$data['page'] = $page; 
		$data['users'] = User::getUserList($name,$start,$this->perPage);
		$data['privilege'] = $this->getPrivilege();
		return view('pages.user',$data);
		//return view('pages.user.inactive',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array();
		$data['status'] = Input::get('status');
		$data['groups'] = IbGroup::all();
		return view('pages.user.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		ini_set('max_execution_time',1000);
		
		$input = Input::all();
		
		$user = new User;
		$user->email = $input['email'];
		$user->user_name = $input['username'];
		$user->first_name = $input['first_name'];
		$user->last_name = $input['last_name'];
		$user->opening_hour_start = str_replace(' ','',$input['opening_hour_start']);
		$user->opening_hour_end = str_replace(' ','',$input['opening_hour_end']);
		$user->shipping_fee = $input['shipping_fee'];
		$user->mobile = $input['mobile'];
		$user->bio = $input['bio'];
		//$user->is_admin = @$input['is_admin'];
		$user->created_at = time();
		$user->timestamps = false;
		
		$profilePic =Input::file('profile_pic');		
		$uploadPath = Config::get('app.upload_path').'profile_image/';
		if (@$profilePic!='')
		{
			$profilePic =Input::file('profile_pic');
			$profilePicImg = "profile_image_".Session::get('id')."_".time().".".$profilePic->guessClientExtension();
			$profilePic->move($uploadPath, $profilePicImg);
			$user->profile_pic = $profilePicImg;
		}
		
		$password = uniqid();
		$user->passwd = md5($password);
		$user->save();
		
		$userRole = $input['user_role'];
		
		IbUserGroup::updateUserGroup($user->id, $userRole);
		
		$input['password'] = $password;
		$input['url'] = Config::get('app.url');
		/*
		Mail::send('emails.register', $input, function($message) use ($input)
		{
			//print_r($message);die();
			$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
			$message->to($input['email']);
			$message->subject('You instabeli account has been created!');
		});
		*/
		@file_get_contents("http://api.instabeli.com/centrin_cms/public/mail_register/".$user->id."/".$password);
		return redirect('/user/create?status=1');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id=0)
	{
		//echo $id;
		$data = array();
		$data['imageURL'] =  Config::get('app.image_url');
		$detail = User::getUserDetail($id);
		$data['detail'] = $detail[0];
		
		$products =  Product::getProductByUser($id);
		for ($i=0;$i<count($products);$i++)
		{
			$products[$i]->comments = User::getProductComment($products[$i]->id);
		}
		$data['products'] = $products;
		
		return view('pages.user.show',$data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$user = User::find($id);
		$data['user'] = $user;
		$data['privileges'] = IbUserGroup::getUserGroup($id);
		$data['groups'] = IbGroup::all();
		$status = Input::get('status');
		//$data['banks'] = Bank::all();
		//$data['bankDetail'] = BankDetail::getBankAccountByUser($id);
		$data['imageURL'] =  Config::get('app.image_url');
		$data['status'] = $status; 
		//return view('pages.user.edit',$data);
		return view('pages.user.config',$data);
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();
		$user = User::find($input['id']);
		
		if (@$user->exists)
		{
			$profilePic =Input::file('profile_pic');		
			$uploadPath = Config::get('app.upload_path').'profile_image/';
			if (@$profilePic!='')
			{
				/*
				$profilePicImage = "profile_image_".Session::get('id')."_".time().".".$profilePic->guessClientExtension();
				$result = $profilePic->move($uploadPath,$profilePic);
				$user->profile_pic = $profilePicImage;
				print_r($result);*/
				
				//$uploadPath = Config::get('app.upload_path');;
				$profilePic =Input::file('profile_pic');
				$profilePicImg = "profile_image_".Session::get('id')."_".time().".".$profilePic->guessClientExtension();
				$profilePic->move($uploadPath, $profilePicImg);
				//echo $uploadPath.$profilePicImage;die();
				$user->profile_pic = $profilePicImg;
			}
			$user->user_name = $input['username'];
			$user->first_name = $input['first_name'];
			$user->last_name = $input['last_name'];
			$user->opening_hour_start = str_replace(' ','',$input['opening_hour_start']);
			$user->opening_hour_end = str_replace(' ','',$input['opening_hour_end']);
			$user->shipping_fee = $input['shipping_fee'];
			$user->mobile = $input['mobile'];
			$user->bio = $input['bio'];
			$user->updated_at = time();
			$user->timestamps = false;
			$user->save();
			
			$userRole = $input['user_role'];
		
			IbUserGroup::updateUserGroup($input['id'], $userRole);
		
			return redirect('/user/edit/'.$input['id'].'?status=1');
		}else
		{
			return redirect('/');
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function checkEmail()
	{
		$email = Input::get('email');
		$count = User::where('email','=',$email)->count();
		if ($count>0)
		{
			return array('status'=>1);
		}else
		{
			return array('status'=>0);
		}
	}
	
	
	public function checkPassword()
	{
		$email = Input::get('email');
		$password = Input::get('old_password');
		$detail = User::getUserByEmail($email);
		if (count($detail)>0)
		{
			if (@$detail[0]->passwd==md5($password))
			{
				return array('status'=>1);
			}else
			{
				return array('status'=>0);
			}
		}else
		{
			return array('status'=>0);
		}
	}
	
	public function reset($id)
	{
		$user = User::find($id);
		
		if (@$user->exists)
		{
			$user->timestamps = false;
			$password = uniqid();
			$user->passwd = md5($password);
			$user->save();
			$input =array();
			
			$input['password'] = $password;
			$input['email'] = $user->email;
			$input['url'] = Config::get('app.url');
			
			Mail::send('emails.reset', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to($input['email']);
				$message->subject('Your instabeli password has been reset!');
			});
		}
		
		return redirect($_SERVER['HTTP_REFERER']);
	}

}
