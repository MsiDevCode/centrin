<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bank;
use App\BankDetail;
use App\Product;
use App\Transaction;
use App\User;
use Config;
use URL;
use Input;
use Session;
use Mail;


use Illuminate\Http\Request;

class ProfileController extends Controller {


	public function __construct()
	{
        $this->middleware('login');
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		//
		$id =  Session::get('id');
		$user = User::find($id);
		$data['user'] = $user;
		$status = Input::get('status');
		//$data['banks'] = Bank::all();
		//$data['bankDetail'] = BankDetail::getBankAccountByUser($id);
		$data['imageURL'] =  Config::get('app.image_url');
		$data['status'] = $status; 
		//return view('pages.user.edit',$data);
		return view('pages.profile.config',$data);
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();
		$user = User::find($input['id']);
		
		if (@$user->exists)
		{
			$profilePic =Input::file('profile_pic');		
			$uploadPath = Config::get('app.upload_path').'profile_image/';
			if (@$profilePic!='')
			{
				/*
				$profilePicImage = "profile_image_".Session::get('id')."_".time().".".$profilePic->guessClientExtension();
				$result = $profilePic->move($uploadPath,$profilePic);
				$user->profile_pic = $profilePicImage;
				print_r($result);*/
				
				//$uploadPath = Config::get('app.upload_path');;
				$profilePic =Input::file('profile_pic');
				$profilePicImg = "profile_image_".Session::get('id')."_".time().".".$profilePic->guessClientExtension();
				$profilePic->move($uploadPath, $profilePicImg);
				//echo $uploadPath.$profilePicImage;die();
				$user->profile_pic = $profilePicImg;
			}
			$user->user_name = $input['username'];
			$user->first_name = $input['first_name'];
			$user->last_name = $input['last_name'];
			$user->opening_hour_start = str_replace(' ','',$input['opening_hour_start']);
			$user->opening_hour_end = str_replace(' ','',$input['opening_hour_end']);
			$user->shipping_fee = $input['shipping_fee'];
			$user->mobile = $input['mobile'];
			$user->bio = $input['bio'];
			$user->updated_at = time();
			$user->timestamps = false;
			$user->save();
			$profile_pic=Config::get('app.image_url')."profile_image/".$user->profile_pic;
			Session::put('profile_pic', $profile_pic);
			return redirect('/profile/edit?status=1');
		}else
		{
			return redirect('/');
		}
		
	}
	
	public function changePassword()
	{
		$input = Input::all();
		$data = array();
		$data['status'] = @$input['status'];
		return view('pages.user.changepassword',$data);
	}
	
	
	public function updatePassword()
	{
		$input = Input::all();
		$password = $input['password'];
		$id =  Session::get('id');
		$user = User::find($id);
		if (@$user->exists)
		{
			$user->timestamps = false;
			$user->passwd = md5($password);
			$user->save();
		}
		return redirect('/profile/change_password?status=1');
	}
}