<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use App;
use URL;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;
	
	protected $perPage=10;
	protected $privilegeId;
	
	protected function checkAccessType($accessType=1)
	{
		$email =  Session::get('email');
		if ($email=='')
		{
			header("Location: ".URL::to('/logout'));
			exit();
		}
		$privilegeId = $this->privilegeId;
		$status = 0;
		$redirectUrl = '';
		foreach (@Session::get('privileges') as $row)
		{
			if (($row->id==$privilegeId)&&($row->access_type>=$accessType))
			{
				$status=1;
			}
			if ($row->access_type>0)
			{
				$redirectUrl = URL::to('/'.$row->url);
			}
			
		}
		if ($status==0)
		{
			if ($redirectUrl=='')
			{
				header("Location: ".URL::to('/logout'));
				exit();
			}else
			{
				header("Location: ".$redirectUrl);
				exit();
			}
		}
	}
	
	protected function getPrivilege()
	{
		$privilegeId = $this->privilegeId;
		$returnValue = array();
		$returnValue['access'] = 0;
		$returnValue['data'] = 0;
		foreach (Session::get('privileges') as $row)
		{
			if (($row->id==$privilegeId)&&($row->access_type>0))
			{
				$returnValue['access']=$row->access_type;
				$returnValue['data']=$row->data_type;
			}
		}
		return $returnValue;
	}

}
