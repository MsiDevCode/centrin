<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Category;
use App\Transaction;
use App\User;
use Config;
use URL;
use Session;
use Input;

class CatalogController extends Controller {
	
	protected $perPage=9;
	
	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 7;
		$this->checkAccessType(1);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$name = Input::get('name');
		$categoryId = Input::get('category_id');
		$status = Input::get('status');
		$page = (Input::get('page')=='')?1:Input::get('page');	
		$start = ($page-1)*$this->perPage;
		$data = array();
		$data['imageURL'] =  Config::get('app.image_url');
        $privileges = $this->getPrivilege();
		$isAdmin =  0;
		if ($privileges['data']==1)
			$isAdmin = 1;
                $products = array();
                if ($isAdmin==1)
                    $products = Product::getLatestProduct(0,$name,$categoryId,$status,$start,$this->perPage);
                else
                    $products = Product::getLatestProduct(Session::get('id'),$name,$categoryId,$status,$start,$this->perPage);
		
                
		$data['products'] = $products;
		$data['limit'] = $this->perPage;
		$data['page'] = $page; 
		$data['privilege'] = $this->getPrivilege();
		$data['categories'] = Category::all();
		$data['name'] = $name; 
		$data['status'] = $status; 
		$data['categoryId'] = $categoryId;
		return view('pages.catalog',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function submit()
	{
		$input = Input::all();
		//print_r($input);
		@$products = $input['products'];
		$status = $input['status'];
		$newCategory = $input['new_category'];
		if (@is_array($products))
		{
			Product::updateStatusProduct($status,$products,$newCategory);
		}
		return redirect($_SERVER['HTTP_REFERER']);
	}

}
