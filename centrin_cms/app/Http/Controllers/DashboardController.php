<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\SalesOrder;
use App\Transaction;
use App\User;
use Config;
use URL;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 1;
		$this->checkAccessType(1);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//die(URL::to('/'));
		$data = array();
		/*
		$productSummary = Product::getProductCreationSummary();
		$data['productSummary'] = $productSummary;
		
		
		$userSummary = User::getUserSummary();
		$data['userSummary'] = $userSummary;
		
		$lastLoggedInUsers = User::getLastLoggedInUsers();
		$data['lastLoggedInUsers'] = $lastLoggedInUsers;
		
		$transactionSummary = Transaction::getTransactionSummary();
		$data['transactionSummary'] = $transactionSummary;
		
		$mostActiveUsers = User::getMostActiveUsers();
		$data['mostActiveUsers'] = $mostActiveUsers;
		
		$categorySummaries = Product::getCategorySummary();
		$data['categorySummaries'] = $categorySummaries;
		
		$date = date('Y-m-d',strtotime(date('Y-m-d') . "-7 days"));
	 	$endDate = date('Y-m-d');
	 	
		$counter=0;
		$d1='';
		$d2='';
		$d3='';
		
	 	while (strtotime($date) <= strtotime($endDate)) {
			if ($counter>0)
			{
				$d1.=",";
				$d2.=",";
				$d3.=",";
			}
			$total = 0;
			foreach ($productSummary as $row)
			{
				if ($row->created==$date)
					$total = $row->total;
			}
			$d1.='[new Date(\''.$date.'\').getTime(),'.$total.']';
			
			$total=0;
			foreach ($transactionSummary as $row)
			{
				if ($row->created==$date)
					$total = $row->total;
			}
			$d2.='[new Date(\''.$date.'\').getTime(),'.($total).']';
			
			$total = 0;
			foreach ($userSummary as $row)
			{
				if ($row->created==$date)
					$total = $row->total;
			}
			$d3.='[new Date(\''.$date.'\').getTime(),'.($total).']';
			
	 		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			$counter++;
	 	}
		$d1='['.$d1.']';
		$d2='['.$d2.']';
		$d3='['.$d3.']';
		
		$d1 = 'var d1 ='.$d1.';';
		$d2 = 'var d2 ='.$d2.';';
		$d3 = 'var d3 ='.$d3.';';
		
		$data['d1'] = $d1;
		$data['d2'] = $d2;
		$data['d3'] = $d3;
		
		return view('pages.dashboard',$data);
		*/
		$orders = SalesOrder::getOrderSummary();
		$date = date('Y-m-d',strtotime(date('Y-m-d') . "-7 days"));
	 	$endDate = date('Y-m-d');
		$orderReports =array();
		
	 	while (strtotime($date) <= strtotime($endDate)) {
			$tmp = (object) array('order_date' => $date, 'total'=>0);
			foreach ($orders as $row)
			{
				if ($row->order_date==$date)
					$tmp->total = $row->total;
			}
			array_push($orderReports,$tmp);
	 		$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
		//print_r($orderReports);die();
		$mostActiveUsers = User::getMostActiveUsers();
		$data['mostActiveUsers'] = $mostActiveUsers;
		$data['orderReports'] = $orderReports;
		$data['stockReports'] = Product::getProductStockSummary();
		$data['statusReports'] = Product::getProductStatusSummary();
		$data['categoryReports'] = Product::getCategorySummary();
		return view('pages.centrin',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
