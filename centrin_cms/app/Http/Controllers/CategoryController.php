<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Category;
use App\Transaction;
use App\User;
use Config;
use URL;
use Session;
use Input;

use Illuminate\Http\Request;

class CategoryController extends Controller {

	
	
	public function __construct()
	{
		$this->middleware('login');
		$this->privilegeId = 3;
		$this->checkAccessType(1);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$categorySummaries = Product::getCategorySummary();
		$data['categories'] = $categorySummaries;
		
		$data['privilege'] = $this->getPrivilege();
		$data['publicURL'] =  Config::get('app.public_url');
		return view('pages.category',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$status = Input::get('status');
		$data = array();
		$data['status'] = $status;
		return view('pages.category.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$uploadPath = Config::get('app.cat_upload_path');;
		$img1 =Input::file('img_1');
		$categoryImg = "category_".Session::get('id')."_".time().".".$img1->guessClientExtension();
		$img1->move($uploadPath,$categoryImg);
		$input = Input::all();
		$category = new Category;
		$category->cat_name = $input['cat_name'];
		$category->cat_name_indo = $input['cat_name_indo'];
		$category->image = $categoryImg;
		$category->timestamps = false;
		$category->save();
		return redirect('category/create?status=1');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);
		if (@$category->exists)
		{
			//print_r($product);
			$data = array();
			$categories = Category::all();
			$data['category'] = $category;
			$data['publicURL'] =  Config::get('app.public_url');
			return view('pages.category.edit',$data);
		}
		else
			return redirect('category');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$uploadPath = Config::get('app.cat_upload_path');;		
		$img1 =Input::file('img_1');
		$categoryImg = '';
		
		if (@$img1!='')
		{
			$categoryImg = "category_".Session::get('id')."_".time().".".$img1->guessClientExtension();
			$img1->move($uploadPath,$categoryImg);
		}
		$input = Input::all();
		
		$category = Category::find($input['id']);
		$category->cat_name = $input['cat_name'];
		$category->cat_name_indo = $input['cat_name_indo'];
		
		if ($categoryImg!='')
			$category->image = $categoryImg;
		
		$category->timestamps = false;
		$category->save();
		return redirect('category');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$detail = Category::getCategoryDetail($id);
		if (count($detail)>0)
		{
			$category = Category::find($id);
			if (@$category->exists)
				$category->delete();
		}
		return redirect('category');
	}

}
