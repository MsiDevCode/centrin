<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'LoginController@index');

Route::get('home', 'LoginController@index');
Route::get('welcome', 'WelcomeController@index');
Route::get('dashboard', 'DashboardController@index');
Route::get('login', 'LoginController@index');
Route::post('login/auth', 'LoginController@auth');
Route::get('forget_password', 'LoginController@forgetPassword');
Route::post('login/reset', 'LoginController@resetPassword');
Route::get('logout', 'LoginController@out');
Route::get('product', 'CatalogController@index');
Route::get('product/edit/{id}', 'ProductController@edit');
Route::post('product/update', 'ProductController@update');
Route::get('product/create', 'ProductController@create');
Route::post('product/store', 'ProductController@store');
Route::post('product/repost', 'ProductController@repost');


Route::get('catalog', 'CatalogController@index');
Route::post('catalog/submit', 'CatalogController@submit');

Route::get('category', 'CategoryController@index');
Route::get('category/create', 'CategoryController@create');
Route::post('category/store', 'CategoryController@store');
Route::get('category/edit/{id}', 'CategoryController@edit');
Route::post('category/update', 'CategoryController@update');
Route::get('category/delete/{id}', 'CategoryController@delete');

Route::get('user', 'UserController@index');
Route::get('user/show/{id}', 'UserController@show');
Route::get('user/create', 'UserController@create');
Route::get('user/check', 'UserController@checkEmail');
Route::get('user/check_password', 'UserController@checkPassword');
Route::post('user/store', 'UserController@store');
Route::get('user/reset/{id}', 'UserController@reset');


Route::get('user/edit/{id}', 'UserController@edit');
Route::post('user/update', 'UserController@update');


Route::get('profile/edit/', 'ProfileController@edit');
Route::post('profile/update', 'ProfileController@update');
Route::get('profile/change_password/', 'ProfileController@changePassword');
Route::post('profile/update_password', 'ProfileController@updatePassword');


Route::get('inactive_user', 'InactiveUserController@index');
Route::post('inactive_user/submit', 'InactiveUserController@submit');

Route::get('offer', 'OfferController@index');
Route::get('offer/accept/{id}', 'OfferController@accept');

Route::get('order', 'OrderController@index');
Route::get('order/print/{id}', 'OrderController@printInvoice');
Route::get('order/detail/{id}', 'OrderController@show');
Route::post('order/cancel', 'OrderController@cancel');
Route::post('order/update_status', 'OrderController@updateStatus');


Route::get('test', 'TestController@index');
Route::get('test/email', 'TestController@sendEmail');



Route::get('group', 'GroupController@index');
Route::get('group/edit/{id}', 'GroupController@edit');
Route::get('group/create', 'GroupController@create');
Route::post('group/store', 'GroupController@store');
Route::post('group/update', 'GroupController@update');


Route::get('mail/{id}/{status}', 'MailController@sendEmailNotification');
Route::get('mail_cancel/{id}/{status}', 'MailController@sendEmailNotificationCancel');
Route::get('mail_register/{id}/{password}', 'MailController@registerMail');
Route::get('mail_reset/{id}/{password}', 'MailController@resetMail');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
