<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrderHistory extends Model {

	protected $table = 'sales_order_history';
	//
	
	public static function getHistory($orderId)
	{
		$query = "SELECT soh.*,sos.`name` as order_status FROM sales_order_history soh
					JOIN sales_order_status sos ON soh.`status_id`=sos.`id`
					WHERE soh.`sales_order_id`='$orderId'
					ORDER BY updated_at";
		$results = DB::select( DB::raw($query));
		return $results;
	}

}
