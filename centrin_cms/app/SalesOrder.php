<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model {
	
    protected $table = 'sales_order';

	//
	public static function updateOrderNumber()
	{
		$query = "UPDATE sales_order SET order_number=OCT(id+18082014) WHERE order_number IS NULL";
		DB::statement($query);
	}
	
	public static function getOrders($sellerId=0,$orderNumber='',$orderStatus=0,$from='',$to='',$start=0,$limit=0)
	{
		$query = "SELECT so.*, soi.`product_name`, u.`user_name` AS seller, CONCAT(IFNULL(u.`first_name`,''),' ',
					IFNULL(u.`last_name`,'')) AS seller_name, u.`email` AS seller_email, 
					sos.`name` AS order_status FROM sales_order so
					JOIN sales_order_item soi ON so.`id`=soi.`order_id`
					JOIN product p ON soi.`product_id`=p.`id`
					JOIN users u ON p.`user_id`=u.`id`
					JOIN sales_order_status sos ON so.`status_id`=sos.`id`
					WHERE 1=1 ";
		if ($sellerId!='0')
		{
				$query .= " AND u.id='$sellerId' ";
		}
		if ($orderNumber!='')
		{
				$query .= " AND so.order_number LIKE '%$orderNumber%' ";
		}
		if ($orderStatus!='0')
		{
				$query .= " AND so.status_id='$orderStatus' ";
		}
		if ($from!='')
		{
			$query .= " AND DATE(so.created_at)>='$from'";
		}
		if ($to!='')
		{
			$query .= " AND DATE(so.created_at)<='$to'";
		}
		if ($sellerId!='0')
			$query .= "	GROUP BY so.`id` ORDER BY sos.priority, so.id ";
		else
			$query .= "	GROUP BY so.`id` ORDER BY  so.id DESC ";
		if ($limit!=0)
		{
				$query .= "	LIMIT $start,$limit";
		}//die($query);
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function createLog($id)
	{
		$query = "INSERT INTO sales_order_history (SELECT '',so.* FROM sales_order so WHERE so.id='$id')";
		DB::statement($query);
	}
	
	public static function getOrderSummary($interval=7)
	{
		$query = "SELECT DATE(so.`created_at`) AS order_date, COUNT(*) AS total
					FROM sales_order so
					WHERE DATE(so.`created_at`)>=DATE(SUBDATE(NOW(), $interval))
					GROUP BY DATE(so.`created_at`)";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	

}
