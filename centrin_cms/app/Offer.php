<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

    protected $table = 'product_offer';
	
	
	
	public static function getNewOffer($userId=0,$start,$limit)
	{		
		$query = "SELECT p.id, p.`product_img_1`,p.`product_img_2`,p.`product_img_3`,p.`product_img_4`, 
					p.`product_name`, FROM_UNIXTIME(p.`inserted_on`) AS inserted_on, u.`first_name`, 
					u.`last_name`, u.`profile_pic`, po.`offered_price`, FROM_UNIXTIME(po.`offer_datetime`) AS offered_at,
					uo.`first_name` AS first_name_o, uo.`last_name` AS last_name_o, uo.`email` AS email_o, uo.`profile_pic` as profile_pic_o, uo.id AS id_o, po.id AS offer_id
					FROM product p
					JOIN users u ON p.`user_id`=u.`id`
					JOIN product_offer po ON p.`id`=po.`product_id`
					JOIN users uo ON po.`offerd_user_id`=uo.`id`
					WHERE p.`isActive`=1 AND po.`status`=1 ";
                if ($userId!=0)
                    $query .= " AND p.user_id='$userId' ";
		$query .="              ORDER BY po.`id` DESC
					LIMIT $start,$limit";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getOfferDetail($id)
	{
		$query = "SELECT p.id, p.`product_img_1`,p.`product_img_2`,p.`product_img_3`,p.`product_img_4`, 
					p.`product_name`, FROM_UNIXTIME(p.`inserted_on`) AS inserted_on, u.`first_name`, 
					u.`id` AS user_id, u.`last_name`, u.`profile_pic`, po.`offered_price`, 
					FROM_UNIXTIME(po.`offer_datetime`) AS offered_at,
					uo.`user_name` AS user_name_o, uo.`first_name` AS first_name_o, uo.`last_name` AS last_name_o, 
					uo.`email` AS email_o, uo.`profile_pic` as profile_pic_o, uo.id AS id_o, po.id AS offer_id,u.`user_name`
					FROM product p
					JOIN users u ON p.`user_id`=u.`id`
					JOIN product_offer po ON p.`id`=po.`product_id`
					JOIN users uo ON po.`offerd_user_id`=uo.`id`
					WHERE p.`isActive`=1 AND po.`status`=1 AND po.id='$id'
					ORDER BY po.`id` DESC";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	public static function acceptOffer($id)
	{
		$query = "UPDATE product_offer SET status=2, offer_accepted_time=UNIX_TIMESTAMP() WHERE id='$id'";
		DB::statement($query);
		$query = "UPDATE product p
					JOIN product_offer po ON p.`id`=po.`product_id`
					SET p.`stage` = 0, p.sold_on = UNIX_TIMESTAMP()
					WHERE po.`id`='$id'";
		DB::statement($query);
	}
}
