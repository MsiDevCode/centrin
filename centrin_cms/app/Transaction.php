<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	//
    protected $table = 'Transaction';

	
	
	public static function getTransactionSummary($interval=7)
	{		
		$query = "
					SELECT DATE(FROM_UNIXTIME(t.`DateTimeCreated`))AS created, COUNT(*) AS total
					FROM `Transaction` t
					WHERE t.`DateTimeCreated`>=UNIX_TIMESTAMP(SUBDATE(NOW(), 7))
					GROUP BY DATE(FROM_UNIXTIME(t.`DateTimeCreated`))
					ORDER BY DATE(FROM_UNIXTIME(t.`DateTimeCreated`))";
		$results = DB::select( DB::raw($query));
		return $results;
	}
}
