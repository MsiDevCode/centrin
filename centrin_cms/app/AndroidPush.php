<?php namespace App;

use DB;

class AndroidPush{
	
	public static function sendNotification($user_id, $text, $name, $title = null, $productId = null)
	{
		$value = array();
		$value['status'] = false;
		$query = "SELECT * FROM user_token WHERE user_id='$user_id' AND device_type=1";
		$results = DB::select( DB::raw($query)); 
		if (count($results)>0)
		{
			$GOOGLE_API_KEY = env('GOOGLE_API_KEY');
			if ($results[0]->registration_id=='')
			{
				$value['result'] = "Registration ID is empty";
			}
			else
			{
				$reg_id = array($results[0]->registration_id);
				$message = array($text);
				$message['name'] = $name;
				$message['notify_title'] = $title;
				$message['product_id'] = $productId;
				$message['reg_id'] = $results[0]->registration_id;
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array(
					'registration_ids' => $reg_id,
					'notify_title' => $title,
					'product_id' => $productId,
					'data' => $message
				);
				$headers = array(
					'Authorization: key=' . $GOOGLE_API_KEY,
					'Content-Type: application/json'
				);
				// Open connection
				$ch = curl_init();
				// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					$value['result'] =curl_error($ch);
				}else
				{
					$value['status'] = true;
					$value['result'] = $result;
				}
				curl_close($ch);
			}
		}else
		{
			$value['result'] = 'Not found';
		}
		return $value;
	}
	
	
}