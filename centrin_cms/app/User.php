<?php namespace App;

use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	public static function getMostActiveUsers($limit=10)
	{
		
		$query = "SELECT u.`user_name`, u.`email`, COUNT(*) AS total, u.first_name, u.last_name 
					FROM `users` u
					JOIN product p ON u.`id`=p.`user_id`
					WHERE p.isActive=1
					GROUP BY u.`id`
					ORDER BY COUNT(*) DESC LIMIT $limit";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	public static function getProductComment($productID=0)
	{
		$query = "SELECT pc.`comment_text`, FROM_UNIXTIME(pc.`commented_on`) AS commented_on,
					u.`first_name`, u.`last_name`, u.`profile_pic` 
					FROM product_comment pc
					JOIN users u ON pc.`user_id`=u.`id`
					WHERE pc.`product_id`='$productID'
					ORDER BY pc.`id`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getUserDetail($id)
	{
		$query = "SELECT u.*, COUNT(DISTINCT(f.`id`)) AS total_following,
					COUNT(DISTINCT(f2.`id`)) AS total_follower,
					(SELECT IF(MAX(last_login) IS NULL,'',FROM_UNIXTIME(MAX(last_login)))  FROM user_token ut WHERE u.`id`=ut.user_id) AS last_login
					FROM users u
					LEFT JOIN follow f ON u.`id`=f.`follower_id`
					LEFT JOIN follow f2 ON u.`id`=f2.`following_id`
					WHERE u.id='$id'
					GROUP BY u.`id`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getUserByEmail($email)
	{
		$query = "SELECT * FROM users WHERE email='$email'";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	public static function getUserList($name='',$start=0,$limit=20)
	{
		$query = "SELECT u.id,u.`first_name`, u.`last_name`, u.`profile_pic`, u.`city`, u.`email`, u.is_active
					FROM users u
					WHERE 1=1 ";
		if (trim($name)!='')
				$query.=" AND (email LIKE '%$name%' OR first_name LIKE '%$name%' OR last_name LIKE '%$name%')";
		$query.="	ORDER BY u.`id` DESC
					LIMIT $start,$limit";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getLastLoggedInUsers($limit=5)
	{
		$query = "SELECT u.`user_name`,u.`email`, FROM_UNIXTIME(MAX(ut.`last_login`)) AS last_login FROM users u
					JOIN user_token ut ON u.`id`=ut.`user_id`
					WHERE ut.`last_login` IS NOT NULL
					GROUP BY u.`id`
					ORDER BY MAX(ut.`last_login`) DESC
					LIMIT $limit";
		
		$results = DB::select( DB::raw($query));
		return $results;	
	}
	
	public static function getUserSummary($interval=7)
	{		
		$query = "SELECT DATE(FROM_UNIXTIME(u.`created_at`))AS created, COUNT(*) AS total
					FROM users u
					WHERE u.`created_at`>=UNIX_TIMESTAMP(SUBDATE(NOW(), 7))
					GROUP BY DATE(FROM_UNIXTIME(u.`created_at`))
					ORDER BY DATE(FROM_UNIXTIME(u.`created_at`))";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getInsactiveUser($start=0,$limit=20)
	{
		$query = "SELECT u.*,(SELECT IF(MAX(ut2.`last_login`) IS NULL, '', FROM_UNIXTIME(MAX(ut2.`last_login`))) FROM user_token ut2 WHERE ut2.`user_id`=u.`id`) AS last_login,
					FROM_UNIXTIME(MAX(po.`offer_datetime`)) AS last_offer
					FROM users u
					JOIN product p ON u.`id`=p.`user_id`
					JOIN product_offer po ON p.`id`=po.`product_id` AND po.`status`=1
					WHERE (SELECT IFNULL(MAX(ut.`last_login`),0) FROM user_token ut WHERE ut.`user_id`=u.`id`)+86400<po.`offer_datetime`
					GROUP BY u.`id`
					LIMIT $start, $limit";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function updateStatusUser($status,$users)
	{
		$allUsers = implode(',',$users);
		//die($allUsers);
		$query = "UPDATE users SET is_active='$status' WHERE id IN ($allUsers)";
		DB::statement($query);
		
		$query = "UPDATE product SET isActive='$status' WHERE user_id IN ($allUsers) and stage=1";
		DB::statement($query);
	}

}
