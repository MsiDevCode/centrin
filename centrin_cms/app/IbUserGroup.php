<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class IbUserGroup extends Model {

	public static function updateUserGroup($userId, $groupId)
	{
		$query = "DELETE FROM ib_user_groups WHERE user_id='$userId'";
		DB::statement($query);
		
		$query = "INSERT INTO ib_user_groups 
							SET 
								 user_id = '$userId',
								 group_id = '$groupId',
								 created_at = NOW()";
		DB::statement($query);
	}
	
	public static function getUserGroup($userId)
	{
		$query = "SELECT * FROM ib_user_groups WHERE user_id='$userId'";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getUserPrivilege($userId)
	{
		$query = "SELECT ip.*, igp.`access_type`, igp.`data_type`
					FROM ib_privileges ip
					JOIN ib_group_privileges igp ON ip.`id`=igp.`privilege_id` 
					JOIN ib_user_groups iug ON igp.`group_id`=iug.`group_id` AND iug.`user_id`='$userId'";
		$results = DB::select( DB::raw($query));
		return $results;
	}

}
