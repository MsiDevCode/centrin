<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class IbGroupPrivilege extends Model {

	public static function getPrivilegeByGroup($groupId)
	{
		$query = "SELECT ip.*, igp.`access_type`, igp.`data_type` FROM ib_privileges ip
					LEFT JOIN ib_group_privileges igp ON ip.`id`=igp.`privilege_id` AND igp.`group_id`='$groupId' ";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function createGroupPrivilege($groupId, $data)
	{
		foreach ($data as $row)
		{
			$query = "INSERT INTO ib_group_privileges
							SET 
								group_id = '$groupId',
								privilege_id = '".$row->privilege_id."',
								data_type = '".$row->data_type."',
								access_type = '".$row->access_type."',
								created_at = NOW() 
							ON DUPLICATE KEY UPDATE
								data_type = '".$row->data_type."',
								access_type = '".$row->access_type."',
								updated_at = NOW() 
								";
			DB::statement($query);
		}
	}

}
