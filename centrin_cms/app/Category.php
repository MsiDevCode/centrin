<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	//
    protected $table = 'category';
	
	public static function getCategoryDetail($id)
	{
		$query = "SELECT c.`cat_name`, c.`cat_name_indo`, c.`image`, SUM(IF(p.`id` IS NULL,0,1)) AS total FROM category c
					LEFT JOIN  product p ON c.`id`=p.`cat_id` AND p.isActive=1
					WHERE  c.id='$id'
					GROUP BY c.`id`
					HAVING SUM(IF(p.`id` IS NULL,0,1))=0";
		$results = DB::select( DB::raw($query));
		return $results;
	}

}
