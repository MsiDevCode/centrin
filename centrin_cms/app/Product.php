<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	//
    protected $table = 'product';
	
	public static function getProductByDate($date='')
	{
		if ($date=='')
			$date=date('Y-m-d');
		
		$nextDate = date('Y-m-d',strtotime($date . "+1 days"));
		
		$query = "SELECT * FROM product p 
					WHERE p.`inserted_on`>=UNIX_TIMESTAMP('$date') 
						AND p.`inserted_on`<UNIX_TIMESTAMP('$nextDate')";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	public static function getProductCreationSummary($interval=7)
	{		
		$query = "SELECT DATE(FROM_UNIXTIME(p.`inserted_on`))AS created, COUNT(*) AS total
					FROM product p
					WHERE p.`inserted_on`>=UNIX_TIMESTAMP(SUBDATE(NOW(), 7))
					GROUP BY DATE(FROM_UNIXTIME(p.`inserted_on`))
					ORDER BY DATE(FROM_UNIXTIME(p.`inserted_on`))";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	public static function getCategorySummary()
	{
		
		$query = "
					SELECT c.id,c.`cat_name`, c.`cat_name_indo`, c.`image`, SUM(IF(p.`id` IS NULL,0,1)) AS total FROM category c
					LEFT JOIN  product p ON c.`id`=p.`cat_id` AND p.isActive=1
					GROUP BY c.`id`
					ORDER BY SUM(IF(p.`id` IS NULL,0,1))DESC";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	
	
	public static function getLatestProduct($userId=0,$name='',$categoryId='',$status='',$start=0, $limit=10)
	{
		$query = "SELECT p.id, p.`product_img_1`,p.`product_img_2`,p.`product_img_3`,p.`product_img_4`, p.`price`, 
					p.`product_name`, FROM_UNIXTIME(p.`inserted_on`) AS inserted_on, p.isActive, u.`first_name`, 
					u.`last_name`, u.`profile_pic`, c.cat_name AS category, p.is_blocked
					FROM product p
					JOIN category c ON p.cat_id=c.id
					JOIN users u ON p.`user_id`=u.`id`
                                        WHERE 1=1 ";
		if ($name!='')
		{
			$query .= " AND p.product_name LIKE '%$name%' ";
		}
		if ($categoryId!='')
		{
			$query .= " AND p.cat_id = '$categoryId' ";
		}
		if ($status!='')
		{
			$query .= " AND p.isActive = '$status' ";
		}
                
                if ($userId!=0)
                {
                        $query .= " AND p.user_id='$userId' ";
                }
                
		$query .= "	ORDER BY p.`id` DESC
					LIMIT $start,$limit";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function repost($id,$total)
	{
		$tmpHash = uniqid();
		for ($i=0;$i<$total;$i++)
		{
			$query = "INSERT INTO product (cat_id, user_id, price, stage, product_name, likes, city, state, 
						description,product_img_1, product_img_2,product_img_3,product_img_4, is_wanted, stock_type, stock_amount, location,
						lat, `long`, inserted_on, sold_on, updated_on, share_facebook, share_twitter, productHash, isActive)
						SELECT cat_id, user_id, price, '1', product_name, '0', city,state,
						description,product_img_1, product_img_2,product_img_3,product_img_4, is_wanted, stock_type, stock_amount, location,
						lat, `long`, UNIX_TIMESTAMP(),0,UNIX_TIMESTAMP(), 0, 0,'$tmpHash',1 FROM product
						WHERE id='$id'";
			DB::statement($query);
		}
		$query = "UPDATE product SET productHash=MD5(id) WHERE productHash='$tmpHash'";
		DB::statement($query);
	}
	
	
	public static function getProductByUser($user_id=0)
	{
		$query = "SELECT p.id, p.`product_img_1`,p.`product_img_2`,p.`product_img_3`,p.`product_img_4`, 
					p.`product_name`, FROM_UNIXTIME(p.`inserted_on`) AS inserted_on, u.`first_name`, 
					u.`last_name`, u.`profile_pic`
					FROM product p
					JOIN users u ON p.`user_id`=u.`id`
					WHERE u.id='$user_id'
					ORDER BY p.`id` DESC";
		$results = DB::select( DB::raw($query));
		return $results;
	} 
	
	public  function createHash($text, $productID)
	{
		$hash_keys = $this->get_hashtags($text);
		if (@$hash_keys!='')
		{
			foreach ($hash_keys as $word) {
	
	
				$checkHashQ = DB::select("SELECT id FROM `product_hash_map` WHERE product_id = $product_id AND hash_text = '$word' ");
				if (count($checkHashQ==0)) {
					DB::statement("INSERT INTO product_hash_map ( `product_id` , `hash_text` , `time_stamp` ) VALUES ($product_id , '$word' , " . time() . " ) ");
				}
			}
		}

	}
	
	
    public function get_hashtags($string, $str = 0) {
        preg_match_all('/#(\w+)/', $string, $matches);
		//print_r($string);
		//print_r($matches);
		$keywords=array();
		$keyword='';
        $i = 0;
        if ($str) {
            foreach ($matches[1] as $match) {
                $count = count($matches[1]);
                $keywords .= "$match";
                $i++;
                if ($count > $i)
                    $keywords .= ", ";
            }
        } else {
            foreach ($matches[1] as $match) {
                $keyword[] = $match;
            }
            $keywords = $keyword;
        }
        return $keywords;
    }
	
	public static function updateStatusProduct($status,$products,$newCategory)
	{
		
		$allProducts = implode(',',$products);
		//die($allUsers);
		if ($status<2)
		{
			if ($status>0)
				$query = "UPDATE product SET isActive='$status' WHERE id IN ($allProducts)";
			else if ($status==-1)
				$query = "UPDATE product SET is_blocked='1' WHERE id IN ($allProducts)";
			else if ($status==-2)
				$query = "UPDATE product SET is_blocked='0' WHERE id IN ($allProducts)";
		}else
		{
			$query = "UPDATE product SET cat_id='$newCategory' WHERE id IN ($allProducts) ";
		}
		DB::statement($query);
		if ($status==0)
		{
			//$query = "DELETE FROM sales_cart WHERE product_id IN ($allProducts)";
			//DB::statement($query);
		}
	}
	
	public static function getProductStockSummary()
	{
		$query = "SELECT IF(p.`stock_type`=0,'Out of stock','Available') AS stock_type, COUNT(*) AS total 
					FROM product p
					GROUP BY p.`stock_type`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getProductStatusSummary()
	{
		$query = "SELECT IF(p.`isActive`=0,'Inactive','Active') AS `status`, COUNT(*) AS total 
					FROM product p
					GROUP BY p.`isActive`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function removeFromCart($id)
	{
		$query = "DELETE FROM sales_cart WHERE product_id='$id'";
		DB::statement($query);
	}

}
