<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model {

	//
    protected $table = 'sales_order';
	
	
	
	public static function updateOrder($orderId)
	{
		$query = "UPDATE sales_order a SET a.grand_total=(SELECT SUM(b.price*b.quantity) FROM sales_order_item b WHERE a.id=b.order_id)+a.shipping_fee,
					order_number=OCT(id+18082014) WHERE a.id='$orderId' ";
		DB::statement($query);
	}
	


	public static function createLog($id)
	{
		$query = "INSERT INTO sales_order_history (SELECT '',so.* FROM sales_order so WHERE so.id='$id')";
		DB::statement($query);
	}
	
	public static function getOrderHistory($accountId,$orderStatus='',$startDate='',$endDate='',$sortBy='created_at',$sortType='ASC',$start=0,$limit=0)
	{
		$query = "SELECT so.`id`, so.`customer_name`, so.`customer_address`, so.`phone`, so.`order_number`, so.`grand_total`, so.`shipping_fee`, 
					so.`created_at`, sos.`name` AS order_status, CONCAT(IFNULL(u.`first_name`,''),' ',IFNULL(u.`last_name`,'')) AS seller_name, 
					u.`email` AS seller_email
					FROM sales_order so
					JOIN sales_order_status sos ON so.`status_id`=sos.`id`
					JOIN sales_order_item soi ON so.`id`=soi.`order_id`
					JOIN product p ON soi.`product_id`=p.`id`
					JOIN users u ON p.`user_id`=u.`id`
					WHERE so.`user_id`='$accountId' ";
		
		if (($startDate!='')&&($endDate!=''))
			$query .= " AND DATE(so.created_at) BETWEEN '$startDate' AND '$endDate' ";
		if ($orderStatus!='')
			$query .= "AND so.`status_id`='$orderStatus' ";
		$query .= "	GROUP BY so.`id` ";
		
		$query .= " ORDER BY $sortBy $sortType ";
		if ($limit!=0)
		{
			$query .= " LIMIT $start,$limit";
		}//die($query);
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function countOrderHistory($accountId,$orderStatus='',$startDate='',$endDate='')
	{
		$query = "SELECT COUNT(distinct(so.id)) AS total 
		FROM sales_order so
					JOIN sales_order_status sos ON so.`status_id`=sos.`id`
					JOIN sales_order_item soi ON so.`id`=soi.`order_id`
					JOIN product p ON soi.`product_id`=p.`id`
					JOIN users u ON p.`user_id`=u.`id`
					WHERE so.`user_id`='$accountId' ";
		if (($startDate!='')&&($endDate!=''))
			$query .= " AND DATE(so.created_at) BETWEEN '$startDate' AND '$endDate' ";
		if ($orderStatus!='')
			$query .= "AND so.`status_id`='$orderStatus' ";
		$results = DB::select( DB::raw($query));
		return $results[0]->total;
	}
	
	public static function getOrderDetail($id)
	{
		$query = "SELECT so.*, sos.`name` AS order_status FROM sales_order so 
					JOIN sales_order_status sos ON so.`status_id`=sos.`id` 
					WHERE so.id='$id' OR order_number='$id'";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function getNotificationQueue()
	{
		$query = "SELECT * FROM sales_order so
					LEFT JOIN app_person_detail apd ON so.`user_id`=apd.`id_person`
					WHERE so.`notification_sent`=0
					ORDER BY so.`id`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function updateNotificationSent($id)
	{
		$query = "UPDATE sales_order SET notification_sent=1 WHERE id='$id'";
		DB::statement($query);
	}

}
