<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	
	public static function getSellers($categoryId='',$key='',$sortBy,$sortType,$start=0,$limit=0)
	{
		$query = "	SELECT u.id AS seller_id, u.`user_name`, u.`first_name`, u.`last_name`, u.`email`, u.`mobile`, u.`profile_pic`,
					u.`opening_hour_start`, u.`opening_hour_end`, u.`shipping_fee`, u.`bio`, SUM(IF(p.`isActive`=1 AND   p.is_blocked=0,1,0)) AS total_items
					FROM users u
					JOIN product p ON u.`id`=p.`user_id`
					JOIN category c ON p.`cat_id`=c.`id`
					WHERE 1=1 ";
		if ($categoryId!='')
		{
			$query.=" AND c.id = '$categoryId' ";
		}
		if ($key!='')
		{
			$query.=" AND (u.first_name LIKE '%$key%' OR u.last_name LIKE '%$key%' OR u.email LIKE '%$key%' ) ";
		}
		$query .= "	GROUP BY u.`id`";
		$query .= " ORDER BY $sortBy $sortType";
		if ($limit!=0)
		{
			$query .= " LIMIT $start,$limit";
		}
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function countSellers($categoryId='',$key='')
	{
		$query = "	SELECT COUNT(distinct(u.id)) AS total
					FROM users u
					JOIN product p ON u.`id`=p.`user_id`
					JOIN category c ON p.`cat_id`=c.`id`
					WHERE 1=1 ";
		if ($categoryId!='')
		{
			$query.=" AND c.id = '$categoryId' ";
		}
		if ($key!='')
		{
			$query.=" AND (u.first_name LIKE '%$key%' OR u.last_name LIKE '%$key%' OR u.email LIKE '%$key%' ) ";
		}
		$results = DB::select( DB::raw($query));
		return $results[0]->total;
	}
	
	public static function getUserDetail($accessKey)
	{
		$query = "SELECT * FROM app_login_table alt
					JOIN app_person_table apt ON alt.`id_account`=apt.`id_person`
					LEFT JOIN app_person_detail apd ON  alt.`id_account`=apd.`id_person`
					LEFT JOIN app_apartement_data aprd ON alt.`id_account`=aprd.`id_person`
					WHERE access_key='$accessKey'";
		$results = DB::select( DB::raw($query));
		return $results;
	}
	


	public static function getUserById($id)
	{
		$query = "SELECT * FROM app_login_table alt
		JOIN app_person_table apt ON alt.`id_account`=apt.`id_person`
		LEFT JOIN app_person_detail apd ON  alt.`id_account`=apd.`id_person`
		LEFT JOIN app_apartement_data aprd ON alt.`id_account`=aprd.`id_person`
		WHERE alt.`id_account`='$id'";
		//echo $query;
		$results = DB::select( DB::raw($query));
		return $results;
	}
	

}
