<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesCart extends Model {

	//
    protected $table = 'sales_cart';
    
    public static function addToCart($accountId,$productId,$quantity)
    {
    	$query = "SELECT * FROM product WHERE id='$productId' ";
    	$results = DB::select( DB::raw($query));
    	if (count($results)>0)
    	{
	    	$query = "INSERT INTO sales_cart
	    				SET 
	    					account_id='".$accountId."',
	    				 	product_id='".$productId."',
	    				 	quantity='".$quantity."',
	    				 	item_price='".$results[0]->price."',
	    				 	created_at = NOW(),
	    				 	updated_at = NOW()
	    				 ON DUPLICATE KEY UPDATE
	    				 	quantity = ".$quantity."+quantity,
	    				 	item_price='".$results[0]->price."',
	    				 	updated_at = NOW()";
			DB::statement($query);
    	}
    }
    

    public static function updateCart($accountId,$productId,$quantity,$comment)
    {
    	$query = "SELECT * FROM product WHERE id='$productId' ";
    	$results = DB::select( DB::raw($query));
    	if (count($results)>0)
    	{
	    	$query = "INSERT INTO sales_cart
	    				SET
	    					account_id='".$accountId."',
	    				 	product_id='".$productId."',
	    				 	quantity='".$quantity."',
	    				 	comment='".$comment."',
	    				 	created_at = NOW(),
	    				 	updated_at = NOW()
	    				 ON DUPLICATE KEY UPDATE
	    				 	quantity = ".$quantity.",
	    				 	comment='".$comment."',
	    				 	updated_at = NOW()";
	    	DB::statement($query);
    	}
    }
    
    public static function updateCartComment($accountId,$productId,$comment)
    {
    	$query = "SELECT * FROM product WHERE id='$productId' ";
    	$results = DB::select( DB::raw($query));
    	if (count($results)>0)
    	{
	    	$query = "INSERT INTO sales_cart
	    				SET
	    					account_id='".$accountId."',
	    				 	product_id='".$productId."',
	    				 	comment='".$comment."',
	    				 	created_at = NOW(),
	    				 	updated_at = NOW()
	    				 ON DUPLICATE KEY UPDATE
	    				 	comment='".$comment."',
	    				 	updated_at = NOW()";
	    	DB::statement($query);
    	}
    }
    
    public  static function getSeller($accountId)
    {
    	$query = "SELECT u.* FROM sales_cart sc
					JOIN product p ON sc.`product_id`=p.`id`
					JOIN users u ON p.`user_id`=u.`id`
    				WHERE sc.account_id='".$accountId."'
					GROUP BY u.`id`";
		$results = DB::select( DB::raw($query));
		return $results;
    }
    
    public static function getCartItem($accountId)
    {
    	$query = "SELECT sc.item_price, sc.`quantity`, sc.`product_id`, sc.`comment`,p.* FROM sales_cart sc
					JOIN product p ON sc.`product_id`=p.`id`
					WHERE sc.`account_id`='$accountId'";
		$results = DB::select( DB::raw($query));
		return $results;
    }
    
    public static function getCartBySeller($userId,$accountId)
    {
    	$query = "SELECT sc.*, p.`price` FROM sales_cart sc
					JOIN product p ON sc.`product_id`=p.`id`
					JOIN users u ON p.`user_id`=u.`id`
    				WHERE u.id='$userId' AND account_id='$accountId'";
		$results = DB::select( DB::raw($query));
		return $results;
		//$results = DB::select( DB::raw($query));
    }
    
    public static function getTotalItem($accountId)
    {
    	$query = "SELECT COUNT(*) AS total FROM sales_cart WHERE account_id='$accountId'";
    	$results = DB::select( DB::raw($query));
    	return $results[0]->total;
    }
    
    public static function removeItem($productId, $accountId)
    {
    	$query = "DELETE FROM sales_cart WHERE account_id='$accountId' AND product_id='$productId'";
    	DB::statement($query);
    }
    
    public static function deleteItem($accountId,$productId)
    {
    	$query = "DELETE FROM sales_cart WHERE account_id='$accountId' AND product_id='$productId'";
    	DB::statement($query);
    	//echo $query;
    }
    
    public static function updatePrice($accountId)
    {
    	$query = "UPDATE sales_cart sc
					JOIN product p ON sc.`product_id`=p.`id`
					SET sc.`item_price`=p.`price` WHERE sc.account_id='$accountId'";
    	DB::statement($query);
    	//echo $query;
    }


    public static function updateAllPrice()
    {
    	$query = "UPDATE sales_cart sc
    	JOIN product p ON sc.`product_id`=p.`cat_id`
    	SET sc.`item_price`=p.`price` ";
    	DB::statement($query);
    }

}
