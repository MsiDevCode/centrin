<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model {

	//
    protected $table = 'product';
	
	public static function getProducts($key='',$sellerId='',$categoryId='',$stockType='',$sortBy='inserted_on',$sortType='',$start=0,$limit=0)
	{
		$query = "SELECT p.`id`, p.`product_name`, p.`description`, p.`price`, p.`product_img_1`, p.`product_img_2`,
				p.`product_img_3`, p.`product_img_4`, u.id AS seller_id, u.`email` AS seller_name, u.`first_name` AS seler_first_name, 
				u.`last_name` AS seller_last_name,
				u.`user_name` AS seller_user_name, c.`id` AS category_id, c.`cat_name` AS category,
				if (p.`stock_type`=1,'in_stock','out_of_stock') as stock_type,
				u.`opening_hour_start` AS seller_opening_hour_start, u.`opening_hour_end` AS seller_opening_hour_end,
				u.mobile AS phone, u.bio, FROM_UNIXTIME(p.`inserted_on`) AS product_creation_date, u.`profile_pic`
				FROM product p
				JOIN users u ON p.`user_id`=u.`id`
				JOIN category c ON c.`id`=p.`cat_id`
				WHERE p.isActive=1 AND p.is_blocked=0 ";
		
		if ($key!='')
		{
			$query .= " AND p.product_name LIKE '%$key%' ";
		}
		
		if ($sellerId!='')
		{
			$query .= "AND u.id='$sellerId' ";
		}
		
		if ($categoryId!='')
		{
			$query .= "AND c.id='$categoryId' ";
		}
		
		if ($stockType!='')
		{
			$query .= "AND p.stock_type='$stockType' ";
		}
		
		$query .= " ORDER BY $sortBy $sortType";
			
		if ($limit!=0)
		{
			$query .= " LIMIT $start,$limit";
		}
		
		$results = DB::select( DB::raw($query));
		return $results;
	}
	
	public static function countProducts($key='',$sellerId='',$categoryId='',$stockType='')
	{
		$query = "SELECT COUNT(*) as total FROM product p
				JOIN users u ON p.`user_id`=u.`id`
				JOIN category c ON c.`id`=p.`cat_id`
				WHERE p.isActive=1 AND p.is_blocked=0   ";
		
		if ($key!='')
		{
			$query .= " AND p.product_name LIKE '%$key%' ";
		}
		
		if ($sellerId!='')
		{
			$query .= "AND u.id='$sellerId' ";
		}
		
		if ($categoryId!='')
		{
			$query .= "AND c.id='$categoryId' ";
		}
		if ($stockType!='')
		{
			$query .= "AND p.stock_type='$stockType' ";
		}
		$results = DB::select( DB::raw($query));
		$totalRow = $results[0]->total;
		return $totalRow;
	}
	
	public static function getProductDetail($productId)
	{
		$query = "UPDATE product SET product_view=product_view+1 WHERE id='$productId'";
		DB::statement($query);
		$query = "
				SELECT p.`id`, p.`product_name`, p.`description`, p.`price`, p.`product_img_1`, p.`product_img_2`,
				p.`product_img_3`, p.`product_img_4`, u.id AS seller_id, u.`email` AS seller_name, u.`first_name` AS selelr_first_name, 
				u.`last_name` AS seller_last_name,
				u.`user_name` AS seller_user_name, c.`id` AS category_id, c.`cat_name` AS category,
				IF (p.`stock_type`=1,'in_stock','out_of_stock') AS stock_type,
				u.`opening_hour_start` AS seller_opening_hour_start, u.`opening_hour_end` AS seller_opening_hour_end, COUNT(*) AS sold,
				u.mobile AS phone, u.bio				
				FROM product p
				JOIN users u ON p.`user_id`=u.`id`
				JOIN category c ON c.`id`=p.`cat_id`
				LEFT JOIN sales_order_item soi ON p.`id`=soi.`product_id`
				WHERE p.`id`='$productId'
				GROUP  BY p.id ";
		$results = DB::select( DB::raw($query));
		return $results;
	}

}
