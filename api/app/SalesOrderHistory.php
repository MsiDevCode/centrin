<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrderHistory extends Model {

	protected $table = 'sales_order_history';

	public static function getHistory($orderId)
	{
		$query = "SELECT  sos.`name` AS order_status,soh.`updated_at`
					FROM sales_order_history soh
					JOIN sales_order_status sos ON soh.`status_id`=sos.`id`
					WHERE soh.`sales_order_id`=$orderId
					ORDER BY soh.`id`";
		$results = DB::select( DB::raw($query));
		return $results;
	}
}
