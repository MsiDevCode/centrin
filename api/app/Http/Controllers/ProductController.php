<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;
use Input;
use Config;

class ProductController extends Controller {

	
	public function __construct()
	{
		$this->middleware('user.auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//getallheaders
		$key = Input::get('key');
		$sellerId = Input::get('seller_id');
		$categoryId = Input::get('category_id');
		$start = (Input::get('start')=='')?0:Input::get('start');
		$limit = Input::get('limit');
		
		$stockType = Input::get('stock_type');
		
		
		$sortBy = Input::get('sort_by');
		if ($sortBy=='')
			$sortBy = 'inserted_on';
		
		if ($sortBy=='date')
			$sortBy = 'inserted_on';
		
		$sortType = Input::get('sort_type');
		
		if ($stockType=='in_stock')
			$stockType=1;
		if ($stockType=='out_of_stock')
			$stockType=0;
		if ($sortType=='')
			$sortType = 'DESC';
		
		$data = Product::getProducts($key,$sellerId,$categoryId,$stockType,$sortBy,$sortType,$start,$limit);
		$returnValue = array();
		$returnValue['row_count'] = Product::countProducts($key,$sellerId,$categoryId,$stockType);
		$returnValue['image_url'] =  Config::get('app.image_url')."product_image/";
		$returnValue['profile_url'] =  Config::get('app.image_url')."profile_image/";
		$returnValue['rows'] = $data;
		return $returnValue;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$returnValue = array();
		$returnValue['image_url'] =  Config::get('app.image_url')."product_image/";
		$productDetail = Product::getProductDetail($id);
		$returnValue['detail'] = $productDetail;
		return $returnValue;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
