<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Input;
use Config;

class SellerController extends Controller {

	
	public function __construct()
	{
		$this->middleware('user.auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$return = array();
		$return['image_url'] =  Config::get('app.image_url')."profile_image/";
		
		$key = Input::get('key');
		$categoryId = Input::get('category_id');
		$return['row_count'] =  User::countSellers($categoryId,$key);
		$start = (Input::get('start')=='')?0:Input::get('start');
		$limit = Input::get('limit');
		
		$sortBy = Input::get('sort_by');
		if ($sortBy=='')
			$sortBy = 'total_items';
		
		if ($sortBy=='name')
			$sortBy = 'first_name';
		
		$sortType = Input::get('sort_type');
		if ($sortType=='')
			$sortType = 'DESC';
			
		$data = User::getSellers($categoryId,$key,$sortBy,$sortType,$start,$limit);
		$return['rows'] = $data; 
		return $return;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$returnValue = array();
		$seller = User::find($id);
		if (@$seller->exists)
		{
			$returnValue['status'] = 1;
			$returnValue['image_url'] =  Config::get('app.image_url')."profile_image/";
			$returnValue['detail'] = $seller;
			
		}else
		{
			$returnValue['status'] = 0;
			$returnValue['descripton'] = 'Seller was not found!';			
		}
		return $returnValue;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
