<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;
use App\SalesOrder;
use App\SalesOrderItem;
use App\User;
use Input;
use Config;
use App\SalesCart;
use App\App;
use App\SalesOrderHistory;

class OrderController extends Controller {


	public function __construct()
	{
		$this->middleware('user.auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$userDetail = $this->getUserDetail();
		$address = Input::get('address');
		$phone = Input::get('phone');
		$sellerId = Input::get('seller_id');
		//print_r($userDetail);
		$items = SalesCart::getCartBySeller($sellerId,$userDetail->id);
		$sellerDetail = User::find($sellerId);
		$returnValue = array();
		if ((@$sellerDetail->exists)&&(count($items)>0))
		{
			//print_r($sellerDetail);
			//if ((time()>= strtotime(date("Y-m-d")." ".$sellerDetail->opening_hour_start))&&(time()<= strtotime(date("Y-m-d")." ".$sellerDetail->opening_hour_end)))
			if ((intval(date('His'))>=intval(str_replace(':','',$sellerDetail->opening_hour_start)))&&(intval(date('His'))<=intval(str_replace(':','',$sellerDetail->opening_hour_end))))
			{
				//echo "here";
				
				//print_r($sellerDetail);
				$order = new SalesOrder;
				$order->customer_name = $userDetail->name;
				$order->customer_address = $address;
				$order->phone = $phone;
				$order->user_id = $userDetail->id;
				$order->status_id = 1;
				if ($sellerDetail->shipping_fee=='')
					$order->shipping_fee = 0;
				else
					$order->shipping_fee = $sellerDetail->shipping_fee;
				$order->save();
				SalesOrder::createLog($order->id);
				//$returnValue = $order;
				//SalesOrder::updateOrder($order->id);
				
				foreach ($items as $row)
				{
					SalesOrderItem::createOrderItem($order->id,$row->product_id,$row->quantity,$row->comment,$row->item_price);
					SalesCart::removeItem($row->product_id, $userDetail->id);
				}
				SalesOrder::updateOrder($order->id);
				
				$returnValue = array();
				$returnValue['status'] = 1;
				$returnValue['order_detail'] = SalesOrder::find($order->id);
			}else {
				$returnValue['status'] = 0;
				$returnValue['description'] = "Seller opening time ".$sellerDetail->opening_hour_start." - ".$sellerDetail->opening_hour_end;
			}
		}else
		{
			$returnValue['status'] = 0;
			$returnValue['description'] = "Invalid Seller ID!";
		}
		return $returnValue;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$orderDetail = SalesOrder::getOrderDetail($id);
		$returnValue = array();
		if (count($orderDetail)>0)
		{
			$returnValue['status'] = 1;
			$returnValue['profile_url'] =  Config::get('app.image_url')."profile_image/";
			$returnValue['order_detail'] = $orderDetail[0];
			$returnValue['order_items'] = SalesOrderItem::getOrderItems($orderDetail[0]->id);
			$returnValue['order_history'] = SalesOrderHistory::getHistory($orderDetail[0]->id);
		}else
		{
			$returnValue['status'] = 0;
			$returnValue['description'] = "Order was not found!";
		}
		return $returnValue;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getHistory()
	{
		$userDetail = $this->getUserDetail();

		$start_date = Input::get('start_date');
		$end_date = Input::get('end_date');
		$sellerId = Input::get('seller_id');
		$start = (Input::get('start')=='')?0:Input::get('start');
		$limit = Input::get('limit');
		$sortBy = Input::get('sort_by');
		if ($sortBy=='')
			$sortBy = 'created_at';
		
		if ($sortBy=='date')
			$sortBy = 'created_at';

		$sortType = Input::get('sort_type');
		
		$orderStatus = Input::get('order_status');
		
		if ($orderStatus=='new')
			$orderStatus=1;
		else if ($orderStatus=='canceled')
			$orderStatus=2;
		else if ($orderStatus=='delivered')
			$orderStatus=3;
		else if ($orderStatus=='expired')
			$orderStatus=4;
		else if ($orderStatus=='in-progress')
			$orderStatus=5;
		
		$data = SalesOrder::getOrderHistory($userDetail->id,$orderStatus,$start_date,$end_date,$sortBy,$sortType,$start,$limit);
		$returnValue = array();
		$returnValue['status'] = 1;
		$returnValue['row_count'] = SalesOrder::countOrderHistory($userDetail->id,$orderStatus,$start_date,$end_date);
		$returnValue['data'] = $data;
		return $returnValue;
	}

}
