<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SalesCart;
use App\SalesOrder;
use App\SalesOrderItem;
use App\User;
use Input;
use Config;
use Mail;


use Illuminate\Http\Request;

class CronController extends Controller {

	public static function sendOrderNotification()
	{
		ini_set('max_execution_time',100000);
		$data = SalesOrder::getNotificationQueue();
		foreach ($data as $row)
		{
			$items = SalesOrderItem::getOrderItems($row->id);
			$input = array();
			$input['order'] = $row;
			$input['items'] = $items;
			@$input['seller_email'] = $items[0]->seller_email;
			$userDetail = User::getUserById($row->user_id);
			print_r($userDetail);
			@$input['user_email'] = $userDetail[0]->email;
			print_r($input);
			
			Mail::send('emails.buyernotification', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				//$message->to('listian.pratomo@gmail.com');
				$message->to($input['user_email']);
				$message->subject('Thank you for your order('.$input['order']->order_number.')!');
			});
			Mail::send('emails.sellernotification', $input, function($message) use ($input)
			{
				//print_r($message);die();
				$message->from('service.instabeli@gmail.com', 'Instabeli Service App');
				$message->to('christiankhoe91@gmail.com');
				//$message->to('listian.pratomo@gmail.com');
				//$message->to($input['seller_email']);
				$message->subject('You have new order ('.$input['order']->order_number.')!');
			});
		
			SalesOrder::updateNotificationSent($row->id);
			//die();
		}
	}
	
	public static function updatePrice()
	{
		SalesCart::updateAllPrice();
	}
	
}