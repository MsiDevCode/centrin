<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SalesCart;
use Input;
use Config;



use Illuminate\Http\Request;

class CartController extends Controller {


	public function __construct()
	{
		$this->middleware('user.auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userDetail = $this->getUserDetail();
		$returnValue =array();
		if ($userDetail->status==1)
		{
			$returnValue['image_url'] =  Config::get('app.image_url')."product_image/";
			$returnValue['profile_url'] =  Config::get('app.image_url')."profile_image/";
			$returnValue['total'] = SalesCart::getTotalItem($userDetail->id);
			$returnValue['carts'] = array();
			$sellers = SalesCart::getSeller($userDetail->id);
			$items = SalesCart::getCartItem($userDetail->id);
			foreach ($sellers as $row)
			{
				$sellerDetail = (object) array();
				$sellerDetail->seller_id = $row->id;
				$sellerDetail->seller_name = $row->first_name." ".$row->last_name;
				$sellerDetail->seller_email = $row->email;
				$sellerDetail->seller_bio = $row->bio;
				$sellerDetail->shipping_fee = $row->shipping_fee;
				$sellerDetail->opening_hour_start = $row->opening_hour_start;
				$sellerDetail->opening_hour_end = $row->opening_hour_end;
				$sellerDetail->profile_pic = $row->profile_pic;
				$sellerDetail->items = array();
				foreach ($items as $rowI)
				{
					if ($rowI->user_id==$row->id)
					{
						$itemDetail = (object) array();
						$itemDetail->product_id = $rowI->id;
						$itemDetail->product_name = $rowI->product_name;
						$itemDetail->price = $rowI->item_price;
						$itemDetail->quantity = $rowI->quantity;
						$itemDetail->description = $rowI->description;
						$itemDetail->comment = $rowI->comment;
						$itemDetail->product_img_1 = $rowI->product_img_1;
						$itemDetail->product_img_2 = $rowI->product_img_2;
						$itemDetail->product_img_3 = $rowI->product_img_3;
						$itemDetail->product_img_4 = $rowI->product_img_4;
						$stockType = ($rowI->stock_type==1)?'in_stock':'out_of_stock';
						$itemDetail->stock_type = $stockType;
						array_push($sellerDetail->items, $itemDetail);
					}
				}
				array_push($returnValue['carts'], $sellerDetail);
			}
			//print_r($returnValue);
			//die();
		}
		return $returnValue;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function add()
	{
		$userDetail = $this->getUserDetail();
		$returnValue =array();
		$returnValue['status'] = 0;
		if ($userDetail->status==1)
		{
			$productId = Input::get('product_id');
			$quantity = Input::get('quantity');
			if (($productId!='')&&($quantity!=''))
			{
				SalesCart::addToCart($userDetail->id,$productId,$quantity);
				$returnValue = $this->index();
				$returnValue['status'] = 1;
			}else {
				$returnValue['status'] = 0;
				$returnValue['description'] = "product_id and quantity are mandatory!";
			}
		}
		return $returnValue;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{	
		$userDetail = $this->getUserDetail();
		$products = Input::get('products');
		$quantities  = Input::get('quantities');
		$comments = Input::get('comments');
		//print_r($products);
		foreach ($products as $key => $value)
		{
			//echo $key."=>".$value."<br>";
			$productId = $value;
			$quantity = @$quantities[$key];
			$comment = @$comments[$key];
			if ($quantity!='')
			{
				SalesCart::updateCart($userDetail->id,$productId,$quantity,$comment);
			}else {
				SalesCart::updateCartComment($userDetail->id,$productId,$comment);
			}
			
		}
		$returnValue = $this->index();
		$returnValue['status'] = 1;
		return $returnValue;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($productId)
	{
		$userDetail = $this->getUserDetail();
		SalesCart::deleteItem($userDetail->id,$productId);
		$returnValue = array();
		$returnValue['status'] = 1;
		$returnValue['description'] = 'Product removed from cart.';
		return $returnValue;
	}
	
	public function updatePrice()
	{
		$userDetail = $this->getUserDetail();
		//$userDetail->id;
		SalesCart::updatePrice($userDetail->id);

		$returnValue['status'] = 1;
		$returnValue['description'] = 'Price updated!.';
		return $returnValue;
	}
	
	

}
