<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;
	
	protected function getUserDetail()
	{
		$header = getallheaders();
		$data = User::getUserDetail($header['Access-Key']);
		$userDetail = (object) array();
		if (count($data)==0)
		{
			$userDetail->status = 0;
		}else 
		{
			$userDetail = $data[0];
			$userDetail->id = $userDetail->id_person;
			$userDetail->status = 1;
		}
		return $userDetail;
	}

}
