<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('product', 'ProductController@index');
Route::get('product/detail/{id}', 'ProductController@show');

Route::get('category', 'CategoryController@index');

Route::get('seller', 'SellerController@index');
Route::get('seller/detail/{id}', 'SellerController@show');

Route::post('order/create', 'OrderController@create');


Route::get('cart', 'CartController@index');
Route::get('cart/update_price', 'CartController@updatePrice');
Route::post('cart/add', 'CartController@add');
Route::post('cart/update', 'CartController@update');
Route::delete('cart/delete/{id}', 'CartController@destroy');



Route::post('checkout', 'OrderController@create');
Route::get('order', 'OrderController@getHistory');
Route::get('order/detail/{id}', 'OrderController@show');


Route::get('user', 'UserController@show');


Route::get('cron/order_notification', 'CronController@sendOrderNotification');
Route::get('cron/price', 'CronController@updatePrice');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
