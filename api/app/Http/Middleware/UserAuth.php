<?php namespace App\Http\Middleware;

use Closure;
use App\User;

class UserAuth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$header = getallheaders();
		//print_r($header);die();
		if (@$header['Access-Key']=='')
		{
			$data['error'] = 'Empty access key."}';
			return $data;
		}else{
			$userDetail = User::getUserDetail($header['Access-Key']);
			if (count($userDetail)==0)
			{
				$data['error'] = 'Invalid access key."}';
				return $data;
			}else {
				return $next($request);
			}
		}
	}

}
