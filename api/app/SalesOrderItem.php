<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrderItem extends Model {

	//
    protected $table = 'sales_order_item';
	
	public static function createOrderItem( $orderId, $productId, $quantity, $comment,$price)
	{
			$query = "INSERT INTO sales_order_item(order_id, product_name, description, price, product_id, 
			created_at, updated_at,quantity,product_img_1, product_img_2, product_img_3, product_img_4,comment) SELECT '$orderId', product_name, description, '$price', id, NOW(), NOW(),
			' $quantity',product_img_1, product_img_2, product_img_3, product_img_4,'$comment'
			FROM product WHERE id = '$productId' ";
			DB::statement($query);
	}
	
	public static function getOrderItems($orderId)
	{
		$query = "SELECT soi.*,  u.id AS seller_id, u.`first_name` seller_first_name, u.`last_name` seller_last_name, 
				u.`email` seller_email, u.`mobile` seller_mobile, u.`profile_pic` seller_profile_pic,
				u.`opening_hour_start`, u.`opening_hour_end`, u.`shipping_fee` seller_shipping_fee, u.`bio` seller_bio 
				FROM sales_order_item soi 
				JOIN product p ON soi.`product_id`=p.`id`
				JOIN users u ON p.`user_id`=u.`id` WHERE order_id='$orderId'";
		$results = DB::select( DB::raw($query));
		return $results;
	}

}
